<? 
//initialise session this will be presenbt for all site files 
session_start();

//error -- success flags 
$success='';
$errorflag='';

//debug flag
$debugmode='ON';

//check flag to switch on usefull error reporting 
if(isset($debugmode) && $debugmode=='ON') {
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

}

//main db connect file
include ('parse_config.php'); 

//set date and timezone defualt 
date_default_timezone_set('Asia/Singapore');
$d = date("Y-m-d H:i:s");

//the base url for site
$base = "http://$domain";


?>

