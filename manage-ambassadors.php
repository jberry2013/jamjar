<?php
//page id variable 
$pageid='manage-ambassadors';
//core vars and logic
include ('includes/application_top.php');	

//if form is submitted load processing script
if(isset($_POST['create']))  {
include ('includes/process_create_ambassador.php');	
}

if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='deactivate' )  {
	//process the deactivate
	include('includes/deactivate.php');
}
if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='activate' )  {
	//process the activate
   include('includes/activate.php');
}

if(isset($_GET['create']))  {
include ('includes/process_create_ambassador.php');	
}
//list of ambassadors
include ('includes/ambassadors_list.php');	
//html header file 
include ('includes/header.php'); ?>          
<body class="hold-transition skin-blue sidebar-mini">   
    <div class="wrapper">
      <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Ambassadors
          </h1>
          <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Manage Ambassadors</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                   <li class="active"><a href="#all_ambassadors" data-toggle="tab">All Ambassadors</a></li>
                  <li><a href="#create_ambassador" data-toggle="tab">Create Ambassador</a></li>
                </ul>
           <div class="tab-content">      
            <div class="active tab-pane" id="all_ambassadors">     
              <div class="box" style="border:none;">
                <div class="box-body">
                  <table id="example1" class="table">
                    <thead>
                      <tr>
                       <th style="width:20%;">Profile Pic</th> 
                        <th style="width:10%;">Name</th>  
                        <th style="width:40%;">Bio</th>
                        <th style="width:5%;">Status</th> 
                        <th style="width:8%;">Access </th>                     
                        <th style="width:7%;">Url</th>
                        <th style="width:10%;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    
                    <?php                
                    if(count($list_contributor_result)>0)  {		
                       foreach ($list_contributor_result as $item ) {  
					   //lets build the vars 
					   
						 $imageName= $item->get("relObjectID")->get('imageName'); 		
						 $name= $item->get("relObjectID")->get('name'); 
						 $bio= $item->get("relObjectID")->get('detail'); 
						 $url=$item->get("relObjectID")->get('URL'); 
						 $item->get("relObjectID")->get('location'); 
						 $location= $item->get("relObjectID")->get('emailAddress'); 
						 $status=$item->get('inactive'); 
						 $access=$item->get('adminAccess'); 
						 $object_ref=$item->get("relObjectID")->getObjectId();
					  
						 
						
						 
						 
					     if($status==true)  {
						  $inactive='SUSPENDED';
						  $class_color2='label-danger';	
						
					      } 
					  
					     if($status==false)  {
						  $inactive='ACTIVE';
						  $class_color2='label-info';	
						  
					     }
						 
					   if($access==false) { 
				
					   $typestring='AMBASSADOR';
					   $class_color='label-primary';	
					   }   
					   if($access==true) {
					   $typestring='ADMINISTRATOR';
				       $class_color='label-success';			
					   }
					  
					   
					   			   	   
					   if (file_exists('../ambassador-images/256/'.$imageName.'.jpg')) { $imagepath='../ambassador-images/256/'.$imageName.'.jpg';	    }  
					   else  { $imagepath='dist/img/no_pic2.png'; }		   
					   ?>
                        <tr>
                        <td><a href="edit-ambassador?ID=<?=$object_ref?>"><img class="img-responsive" src="<?=$imagepath?>" alt="Profile pic"></a></td>
                        <td style="padding-top:20px;"> <a href="#"><?=$name?></a></td>
                        <td style="width:50%;padding-top:20px;"><?=$bio?></td>  
                        <td style="width:5%;padding-top:20px;"><span class="label <?=$class_color2?>"><?=$inactive;?></span> </td>  
                        <td style="width:5%;padding-top:20px;"><span class="label <?=$class_color?>"><?=$typestring;?></span> </td>             
                        <td style="padding-top:20px;"><a target="_blank" href="<?=$url?>"><?=$url?></a> </td>
                        <td style="padding:20px;padding-top:20px;"><a href="edit-ambassador?ID=<?=$object_ref?>"  ><button class="btn btn-block btn-primary" style="padding:2px;"><span class="glyphicon glyphicon-cog"></span>&nbsp;Edit-Details</button></a>
                        <br>
                        <? if($status==true) { ?>
						<a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;" title="This will re-activate the ambassador"><button class="btn btn-block btn-success btn-success"  style="padding:2px;" data-id="<?=$object_ref?>" data-href="manage-ambassadors?ID=<?=$object_ref?>&flag=activate" data-toggle="modal" data-target="#confirm-delete2"><span class="glyphicon glyphicon-ok"></span>&nbsp;Activate</button></a>
                         
                         <? } else if($status==false) { ?> 
                        <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;" title="This will de-activate the ambassador"><button class="btn btn-block btn-warning  btn-confirm"  style="padding:2px;" data-id="<?=$object_ref?>" data-href="manage-ambassadors?ID=<?=$object_ref?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span>&nbsp;De-activate</button></a>
                         <?  }  ?>
                         </td>    
                      </tr>   		   
                     <?  } } ?>               
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->             
                  </div>
                   <div class="tab-pane" id="create_ambassador">
                    <div class="box" style="border:none;">
                   <div class="box-header">
                    <h3 class="box-title">Details </h3>
                  </div><!-- /.box-header -->
                <div class="box-body">
                     <form enctype="multipart/form-data" method="post" id="createuser" action="manage-ambassadors#create_ambassador" >
                    <!-- text input -->
                    <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                       <input type="text" class="form-control" placeholder="name" name="crm_name" id="crm_name" data-msg-required="A name is required." data-rule-required="true" 
                       value="<? if(isset($crm_name)) echo $crm_name ?>">
                    </div>
                    <br>
                    
                    <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                       <input type="text" class="form-control" placeholder="email" name="crm_email" id="crm_email" data-msg-required="An email is required." data-rule-required="true"
                          value="<? if(isset($crm_email)) echo $crm_email ?>">
                    </div>
                    <br>
                       <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                       <input type="text" class="form-control" placeholder="location Autosearch your location by starting to type." name="crm_location" id="crm_location" 
                       data-msg-required="A location is required" data-rule-required="true" value="<? if(isset($crm_location)) echo $crm_location ?>">
                    </div>
                    <br>
                     <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                       <input type="text" class="form-control" placeholder="http://" name="crm_url" id="crm_url" data-val-url="true" data-rule-required="true"    
                       value="<? if(isset($crm_url)) echo $crm_url ?>">
                    </div>
                    <br>
                      <div class="form-group" style="float:left;">
                      <label for="exampleInputFile">CRM Access Level </label>
                       <p class="help-block">check the box below if you want to grant this user admin level access or leave blank for regular ambassador access</p>
               <div class="checkbox icheck">
                <label>
                  <input type="checkbox"  name="access" value="yes" id="admin" class="role" <? if(isset($admin_level) && $admin_level==true) echo "checked='checked'" ?>>&nbsp; Admin level access
                </label>
              </div>
                    </div>
                    <br>    
                   <br style="clear:both">  
                   <div class="form-group">
                      <label for="exampleInputFile">Upload an Image / profile pic </label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                    </div>               
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title"> bio / description </h3>
                </div><!-- /.box-header -->
            <div class="form-group">     
            <textarea class="form-control" rows="5" name="crm_bio" id="crm_bio" placeholder="Enter a description here  (No more than 100 words)..."><? if(isset($crm_bio)) echo $crm_bio ?></textarea>
              <p class="help-block" id="chars">500</p>
            </div>
               <div class="form-group">
                        <div class="col-sm-1" style="padding-left:0;">
                          <button type="submit" name="create" class="btn btn-success">Create Ambassador</button>
                        </div>
                        <!--   <div class="col-sm-2" style="padding-left:50px;">
                          <button type="submit" name="create" class="btn btn-danger reset">Reset Form</button>
                        </div>-->
                      </div> 
                  </form>
                  </div><!-- /.box -->              
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->     
        <div class="example-modal5" style="display:none;" id="confirm-delete">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will de-activate the ambassador / user</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
            <div class="example-modal6" style="display:none;" id="confirm-delete2">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will re-activate the ambassador / user</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
      
      
  <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
        <? }  
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>   
      <script>
      $(function () {
        $("#example1").DataTable({ "iDisplayLength": 5 ,  "aLengthMenu": [5, 10, 25, 50, 100]});
		  $('.dataTables_filter input').attr("placeholder", "Search by any of the displayed columns here to sort the list");
		   $('.dataTables_filter input').css("width", "350px");
        $('#example2').DataTable({	
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>  
<script>
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

</script>  
<script>
var elem = $("#chars");
$("#crm_bio").limiter(500, elem);
</script>
   <script>
      $(function () {
        $('#crm_location').geocomplete();
      });
    </script>  
    <script type="text/javascript">
    $("#createuser").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>
    <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>
 
 
   <script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Ambassador ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    
     <script>
	$('#confirm-delete2').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Ambassador ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    
        
   <script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
    
    });
});
</script>

 
 
 <script>
 // Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})
</script>

  <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
 </body>
</html>