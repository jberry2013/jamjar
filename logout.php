<?php 
session_start();
$domain =  $_SERVER['SERVER_NAME'];
$logout_link = "http://$domain/admin-crm";

if(isset($_SESSION['login_username']) && isset($_SESSION['login_password']))  {
   
   unset($_SESSION['name']);
   unset($_SESSION['bio']);
   unset($_SESSION['url']);
   unset($_SESSION['createdAt']);
   unset($_SESSION['login_username']);
   unset($_SESSION['login_password']);
   session_destroy();
   header('Location: '.$logout_link);
}

else  {
   session_destroy();
   header('Location: '.$logout_link);
	
}
?>
