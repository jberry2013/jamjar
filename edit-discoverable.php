<?php
//page id variable 
$pageid='manage-discoverables-edit';
$subpageid='edit-discoverable';
//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;


//core vars and logic
include'includes/application_top.php';	
//html header file 
include ('includes/header.php');


//if form is submitted load processing script
if(isset($_POST['edit']))  {
include ('includes/process_edit_discoverable.php');	
}

if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='deactivate' )  {
    include('includes/deactivate_discoverable.php');
}
if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='activate' )  {
  	include('includes/activate_discoverable.php');
}


//list of intesrests
$query_new = new ParseQuery("Interest");
$query_list_tags1 = $query_new->find();


//list of browsing tags
$query_new2 = new ParseQuery("browsingTags");
$query_list_tags2 = $query_new2->find();


//list of ambassadors
include ('includes/discoverable_data.php');	
?>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
  <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Discoverable
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
             <? if($_SESSION['admin_level']==true) { ?>
                <li><a href="manage-discoverables"><i class="fa fa-building"></i> Manage Discoverables</a></li>
             <?  }  else { echo ' <li><a href="manage-discoverables?admin=n"><i class="fa fa-building"></i> Manage Discoverables</a></li>';  } ?>
            <li class="active">Edit Discoverable</li>
          </ol>
        </section>
        
     
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                 <img class="img-responsive" src="<?=$imagepath2?>" width="360px" height="360px" alt="Photo" >        
                </div><!-- /.box-body -->
              </div><!-- /.box -->


            </div><!-- /.col -->
            
             <!-- About Me Box -->
           
            <div class="col-md-9">
               <div class="box box-primary">
            
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Discoverable</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
            
                  <form enctype="multipart/form-data" method="post" id="editdiscoverable" action="edit-discoverable">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Discoverable Title</label>
                      <input type="text" class="form-control" id="title" name="title" value="<? if(isset($title)) echo $title ?>" placeholder="Enter an approriate title   (No more than 100 characters)..."  data-msg-required="A title is required." data-rule-required="true" >
                        <input type="hidden" class="form-control"  name="ID" id="ID"  value="<? if(isset($object_ref)) echo $object_ref ?>">
                         <input type="hidden" class="form-control" name="am_image" id="am_image"  value="<? if(isset($imageName)) echo $imageName ?>">
                          <p class="help-block" id="chars2">100</p>
                    </div>
                    <div class="form-group">
                      <label>Sub Title</label>
                      <input type="text" class="form-control" id="sub_title" name="sub_title" value="<? if(isset($subtitle)) echo $subtitle ?>"  placeholder="Enter a suitable sub-title... (No more than 100 characters)">
                    <p class="help-block" id="chars3">100</p>
                    
                 
                    </div>
                        <? if( $_SESSION['admin_level']==true)  { ?>
                        <div class="form-group">
                        <label>Current ambassador for discoverable</label>
                        <select class="form-control" name="allocated_ambassador" id="allocated_ambassador"  data-msg-required="An ambassador is required." data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <?
                        if(count($query_list_ambassadors)>0)  {		
                        foreach ($query_list_ambassadors as $val ) { 
						
						  $object_ref_amb=$val->getObjectId();
						  $ambassador_name=$val->get('name'); 
						?>   
					   <option value="<?=$object_ref_amb?>" <? if(trim($object_ref_amb)==trim($object_ref_a)) echo 'selected=selected'?>>
					   <?=$ambassador_name?></option>
						<?  } } ?>           
                      </select>
                   </div>
                    <?  }  ?>
                    
                    
                    
                       <div class="form-group">
                       <label>type of discoverable</label>
                     <select class="form-control" name="type" id="type"  data-msg-required="A type of discoverable is required." data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <option value="1" <? if($type==1) echo "selected=selected"?>>Place</option>
                        <option value="2" <? if($type==2) echo "selected=selected"?>>Special</option>
                        <option value="3" <? if($type==3) echo "selected=selected"?>>Event</option>                      
                      </select>
                   </div>
                   
                    <?  if(sizeof($date_array)==0)  {  ?>
					
                   <div id="choose_dates" style="display:none;">
                   
                   <div class="box-header" style="padding-left:0; padding-top:20px;">
                  <h3 class="box-title">Give the Timeslot a label, an expiry date, and select specific dates from the start /  end date calender below </h3>
                   </div><!-- /.box-header -->
                   
                   
                     <div class="form-group">
                    <label>Input a label for the times(s) slot </label>
                      <input type="text" name="timeslotstring" class="form-control" placeholder="Eg. Every Monday And Wednesday @ 8.30-10.30 pm" id="timestring" 
                      value="<? if(isset($date_string)) echo $date_string ?>" autofocus>
                    </div>
                    
                      <div class="form-group">
                    <label>Expiry of special or event</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteexpiry" class="form-control pull-right timeslotsingle" placeholder="please choose an expiry date" 
                     value="<? if(isset($date_expiry)) echo $date_expiry ?>">
                    </div><!-- /.input group -->
                    
                  </div>
                    
                     <div class="form-group">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTime[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_1" class="btn btn-primary" onClick="addSlot1();">add new time slot</button>
                  </div>
                    
         
                    </div>
                    <?  }   
                  
                     if(count($date_array)>0)  {	?>
					 
					   <div class="form-group">
                    <label>label for the times(s) slot </label>
                      <input type="text" name="timeslotstring" class="form-control" placeholder="Eg. Every Monday And Wednesday @ 8.30-10.30 pm" id="timestring" 
                      value="<? if(isset($date_string)) echo $date_string ?>" autofocus>
                    </div>
                    
                      <div class="form-group">
                    <label>Expiry date</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteexpiry" class="form-control pull-right timeslotsingle" placeholder="please choose an expiry date"
                     value="<? if(isset($date_expiry)) echo date('d/m/Y H:i', $date_expiry) ?>">
                    </div><!-- /.input group -->
                    
                  </div>
                     	
                   <?  foreach ($date_array as $slot ) { 
					 $pieces_dte = explode(":", $slot);
					 $slot1_from=$pieces_dte[0];
                     $slot1_to=$pieces_dte[1];
					 ?>
                    <div class="form-group">
                    <label>Date range:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="dteTime[]" class="form-control pull-right datetimeslot" id="date" 
                      value="<?=date('d/m/Y H:i', $slot1_from).' - '.date('d/m/Y H:i', $slot1_to);?>">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                      
                  <?   } echo ' <div class="form-group" id="parentsection">   
                  
                    <button type="button" class="btn btn-primary" onClick="addSlot();">add new time slot</button> 
                   </div>    '; }  ?>
                   
                   
                    
                    <div class="form-group" style="display:none;" id="first">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_1" class="btn btn-primary" onClick="addSlot1();">add new time slot</button>
                  </div>
                  
                  <div class="form-group" style="display:none;" id="second">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_2" class="btn btn-primary" onClick="addSlot2();">add new time slot</button>
                  </div>
                  
                  <div class="form-group" style="display:none;" id="third">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_3" class="btn btn-primary" onClick="addSlot3();">add new time slot</button>
                  </div>
                  
                   <div class="form-group" style="display:none;" id="fourth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_4" class="btn btn-primary" onClick="addSlot4();">add new time slot</button>
                  </div>
                  
                   <div class="form-group" style="display:none;" id="fifth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_5" class="btn btn-primary" onClick="addSlot5();">add new time slot</button>
                  </div>
                 
                  
                      
                  
                  <div class="box-header">
                  <h3 class="box-title">Discoverable Description</h3>
                </div><!-- /.box-header -->
                  <div class="form-group">
                     
                     <textarea class="form-control" id="desc" name="discoverable_desc" rows="5" placeholder="Enter a description here  (No more than 100 words)..." data-msg-required="A description of the discoverable is required." data-rule-required="true"><? if(isset($desc)) echo $desc ?></textarea>
                      <p class="help-block" id="chars">500</p>
                    </div>     
                  
                       
                <div class="box-header" style="margin-left:0px; padding-left:0px;">
                <h3 class="box-title" > <a href="#" class="toggleimage" style="color:#333;"><i class="fa fa-picture-o"></i> Upload a new Image </a></h3>
                </div><!-- /.box-header -->
                
                  
              
                  
    
                    
                   <br style="clear:both">
                   <div class="form-group" id="changepic" style="display:none;">
                  <div class="col-xs-4" style="margin-left:0px; padding-left:0px;"> 
     
                     <label for="exampleInputFile">Browse for image</label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                      </div>
                     <br>

                    </div>               
                    <br style="clear:both;">
             
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title">More details</h3>
                </div><!-- /.box-header -->
                   
                    <div class="form-group">
                      <label>Address </label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="begin typing to auto complete the location" 
                      data-msg-required="An address is required." data-rule-required="true" value="<? if(isset($address)) echo $address?>">
                    </div>
                    <div class="form-group">
                      <label>Location (lattitide and Longitude)</label>
                    </div>
                    
                    <div class="col-xs-3" style="padding-left:0;">
                      <input type="text" class="form-control" name="lat" id="lat" placeholder="latitude.." value="<? if(isset($lat)) echo $lat ?>">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" name="lng" id="lng"  placeholder="longitude..." value="<? if(isset($lng)) echo $lng ?>">
                    </div>
                           
                   <br style="clear:both"><br>
                    
                   <div class="form-group">
                       <label>Jam Jar City</label>
                     <select class="form-control" name="jamjarcity"  id="jamjarcity" data-msg-required="A city is required" data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <option value="Bali" <? if($jamjarcity=='Bali') echo 'selected=selected'?>>Bali</option>
                        <option value="Perth"<? if($jamjarcity=='Perth') echo 'selected=selected'?> >Perth</option>
                                       
                      </select>
                   </div>
                  
                  
                  
                   
                  
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Price</h3>
                   </div><!-- /.box-header -->
                    <div class="form-group">
                      <input type="text" class="form-control" name="cost" id="cost" placeholder="how much" value="<? if(isset($cost)) echo $cost?>" >
                      
                        <input type="hidden" class="form-control" name="ID" id="ID" placeholder="how much" value="<? if(isset($object_ref)) echo $object_ref?>">
                      
                      
                    </div>
                    
                       
                      <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Contact Number</h3>
                   </div><!-- /.box-header -->
                    
                     <div class="form-group">
                      <input type="text" class="form-control" name="contact" id="contact" placeholder="contact number" value="<? if(isset($contact)) echo $contact?>">
                    </div>
                    
                    
                       
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Discoverable Tips</h3>
                   </div><!-- /.box-header -->
                   
                    <div class="form-group">   
                     <?  if(count($tips_array)>0)  {		
                      foreach ($tips_array as $item1 ) { ?>	 
                       <li class="default" style="margin-bottom:10px; list-style-type:none;"> 
                      <input placeholder="" type="text" name="tips[]" value="<?=$item1?>" width="160"/><span style="cursor: pointer;" onClick="closeMe(this);"> <i class="fa fa-close"></i></span>	
                      </li>  
					 <? } } ?>        
                    </div> 
                   
                     <ul id="list2"  style="padding-left:0;">
                    <li class="default" style="display: none; padding-left:0 padding-bottom:10px; list-style-type:none;">
                        <input placeholder="new tip" type="text" name="tipsnew[]"/><span style="cursor: pointer;" onClick="closeMe2(this);"> <i class="fa fa-close"></i></span>
                    </li>
                    </ul>
                    
                                          
                    <div class="form-group">   
                    <button type="button" class="btn btn-primary" onClick="addTip();">add a new tip</button>
                    </div> 
                    
                    

                  <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tags / interests</h3>
                   </div><!-- /.box-header -->
                   <? 
				    $counter=0;
                     if(sizeof($query_list_tags1)>0)  {		
                      foreach ($query_list_tags1 as $item1 ) { 
					  $objectRef= $item1->getObjectId(); 
					  if (($counter + 1) % 6 == 0) {  echo '<br><br><br>'; }		
				   ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="tags[]" value="<?=$objectRef?>" <?
						   if (in_array($objectRef, $object_ref_tags)) echo "checked=checked"; ?>>
                        </span>
                 <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;" title="<?=$item1->get('title')?>">     
                  <input type="text" class="form-control" placeholder="<?=$item1->get('title')?>" readonly></a>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
        
                   
                    <? $counter++; } }  ?>
                     <br style="clear:both"><br>
                    <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Browsing Tags</h3>
                   </div><!-- /.box-header -->
                   
                    <? 
					 $counter=0;
                     if(sizeof($query_list_tags2)>0)  {		
                      foreach ($query_list_tags2 as $item2 ) { 
					  $objectRef= $item2->getObjectId();  
					  if (($counter + 1) % 6 == 0) {  echo '<br><br><br>'; }		
					  
					   ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="tags2[]" value="<?=$objectRef?>"  <? if (in_array($objectRef, $btag_object)) echo "checked=checked"; ?>>
                        </span>
                     <a id="popover" data-toggle="tooltip" data-placement="top"    style="cursor:pointer;" title="<?=$item2->get('title')?>">   
                      <input type="text" class="form-control" placeholder="<?=$item2->get('title')?>" readonly></a>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                 
                   
                    <?   $counter++; } }  ?>
                   
                       
                     <br style="clear:both;"><br>
                     <div class="form-group">         
                     <button type="submit" name="edit" class="btn btn-success btn-lrg">Edit discoverable</button>
                     
                      <? if ($inactive==false)  {  ?>
                           <a id="popover" data-toggle="tooltip" data-placement="top"    style="cursor:pointer;" title="This will deactivate the discoverable ">   &nbsp;&nbsp;
                   <button type="button"class="btn btn-danger btn-lrg"  data-id="<?=$object_ref?>" data-href="edit-discoverable?ID=<?=$object_ref?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-ok"></span> Deactivate</button></a>
                   </a>
                   
                   <?  }  ?>
                        
                        
      <? if ($inactive==true)  {  ?>
                           <a id="popover" data-toggle="tooltip" data-placement="top"    style="cursor:pointer;" title="This will reactivate the discoverable ">   &nbsp;&nbsp;
                   <button type="button"class="btn btn-primary btn-lrg"  data-id="<?=$object_ref?>" data-href="edit-discoverable?ID=<?=$object_ref?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-ok"></span> Deactivate</button></a>
                   </a>
                   
       <?  }  ?>
                     </div>                     
                     </form>
                          
                        
                     
                     
                   
                  
                  
            </div>
            </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
       <div class="example-modal5" style="display:none;" id="confirm-delete">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will de-activate the discoverable</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">De-activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
            <div class="example-modal6" style="display:none;" id="confirm-delete2">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will re-activate the discoverable</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
        
       <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
        <? }  
      
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
   
    <script>
	$(".toggleimage").click(function () {
    $("#changepic").toggle();
     });
    </script>
    
    <script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>

  <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>

 <script type="text/javascript">
    $("#editdiscoverable").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>
<script>
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

</script>  
<script>
var elem = $("#chars");
$("#crm_bio").limiter(500, elem);
</script>

<script>

function closeMe2(element) {
  $(element).parent().remove();
}

function closeMe(element) {
  $(element).parent().remove();
}

function addTip() {
  var container = $('#list2');
  var item = container.find('.default').clone();
  item.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item.appendTo(container).show();
}

	
</script>  

  <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>
<script>
var elem = $("#chars2");
$("#title").limiter(100, elem);
</script>

<script>
var elem = $("#chars3");
$("#sub_title").limiter(100, elem);
</script>
     <script>
var elem = $("#chars");
$("#bio_desc").limiter(500, elem);
</script>
    <script>
      $(function () {		  
		  var mapoptions = {
		  details: "form",
		  types: ['geocode','establishment']
        };
        
		$('#address').geocomplete(mapoptions)		
       
});

  </script> 
  
  
   <script>

function closeMe2(element) {
  $(element).parent().remove();
}

function addTip() {
  var container = $('#list2');
  var item = container.find('.default').clone();
  item.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item.appendTo(container).show();
}


function addSlot() {
  $('#parentsection').hide();
  $('#first').show();
  
}

function addSlot1() {

 $('#button_1').hide();
  $('#second').show();
  
}


function addSlot2() {
 $('#button_2').hide();
  $('#third').show();
  
}

function addSlot3() {
 $('#button_3').hide();
  $('#fourth').show();
  
}

function addSlot4() {
 $('#button_4').hide();
  $('#fifth').show();
  
}

function addSlot5() {
 $('#button_5').hide();
  $('#sixth').show();
  
}


</script>  


  

<script>
  $(function () {
    $('.datetimeslot').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY H:mm'});

    $('.timeslotsingle').daterangepicker({timePicker: true, singleDatePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY H:mm'});
   })
   
   
   
   
    </script>  
    
<script type="text/javascript">
$(document).ready(function(){
    $("#type").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")==2){
                
                $("#choose_dates").show();
            }
            else if($(this).attr("value")==3){
              
                $("#choose_dates").show();
            }
           
            else{
                $("#choose_dates").hide();
            }
        });
    })
});
</script>

    <script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>


<script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Discoverable ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    
     <script>
	$('#confirm-delete2').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Discoverable ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
  
  
</body>
</html>
