<?php
//page id variable 
$pageid='manage-profile';
//core vars and logic
include'includes/application_top.php';	
//html header file 
include ('includes/header.php');

//if form is submitted load processing script
if(isset($_POST['edit']))  {
include ('includes/process_edit_profile.php');	
}

//list of ambassadors
include ('includes/profile_data.php');	

?>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
  <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Profile
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manage Profile</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?=$imagepath2?>" alt="User profile picture">
                  <h3 class="profile-username text-center"><? if(isset($am_name)) echo $am_name; ?></h3>
              <!--    <p class="text-muted text-center">Software Developer</p>-->

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Discoverables Contributed</b> <a class="pull-right"><?=$cnt_discoverables?></a>
                    </li>
                   
                   
                  </ul>

               
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Me</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
               <!--   <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
                  <p class="text-muted">
                    B.S. in Computer Science from the University of Greenwich
                  </p>

                  <hr>
-->
                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted"><? if(isset($am_location)) echo $am_location; ?></p>

                  <hr>

                  <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                  <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Mysql</span>
                  </p>

                  <hr>

                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Biography</strong>
                  <p><? if(isset($am_bio)) $string = (strlen($am_bio) > 50) ? substr($_SESSION['bio'],0,50).'...' : $am_bio; echo $string; ?></p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
               <div class="box box-primary">
            
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Details</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
            
                <form id="edituser"  enctype="multipart/form-data" method="post" action="manage-profile">
                    <!-- text input -->
                    <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                   <input type="text" class="form-control" name="am_name" placeholder="name" value="<? if(isset($am_name)) echo $am_name; ?>">
                   <input type="hidden" class="form-control"  name="ID" id="ID"  value="<? if(isset($_SESSION['object_ref'])) echo $_SESSION['object_ref'] ?>">
                   <input type="hidden" class="form-control"  name="am_image" id="am_image"  value="<? if(isset($_SESSION['userImage'])) echo $_SESSION['userImage'] ?>">  <input type="hidden" class="form-control" placeholder="name of ambassador" name="admin_level" id="admin_level"  value="<? if(isset($_SESSION['admin_level'])) echo $_SESSION['admin_level'] ?> ">
                    </div>
                    <br>
                    
                    <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                       <input type="text" class="form-control" name="am_email" placeholder="email" value="<? if(isset($am_email)) echo $am_email; ?>">
                    </div>
                    <br>
                       <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                       <input type="text" class="form-control"  name="am_location" id="crm_location" placeholder="location " value="<? if(isset($am_location)) echo $am_location; ?>">
                    </div>
                    <br>
                     <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                       <input type="text" class="form-control"  name="am_url" placeholder="URL" value="<? if(isset($am_url)) echo $am_url; ?>">
                    </div>
                    <br>
                      <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                       <input type="text" class="form-control" name="password" placeholder="Change your password" value="<? if(isset($am_password)) echo $am_password; ?>">
                    </div>
                    
                
                  <br style="clear:both;"> 
                    
                <div class="box-header" style="margin-left:0px; padding-left:0px;">
              <h3 class="box-title"> <a href="#" class="toggleimage" style="color:#333;"><i class="fa fa-picture-o"></i> Change Profile Image </a></h3>
                </div><!-- /.box-header -->
            
                    
                 
                   <div class="form-group" id="changepic" style="display:none;">
                  <div class="col-xs-4" style="margin-left:0px; padding-left:0px;"> 
     
                     <label for="exampleInputFile">Browse for image </label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                      </div>
                    </div>               
                  
                   
                
                  <div class="box-header" style="clear:both;margin-left:0px; padding-left:0px;">
                  <h3 class="box-title">Edit your biography </h3>
                </div><!-- /.box-header -->

                  <div class="form-group">   
                  <textarea class="form-control" rows="5" name="am_bio" placeholder="Enter a description here  (No more than 100 words)..."><? if(isset($am_bio)) echo $am_bio; ?></textarea>
                    </div>                  
                     <div class="form-group">
                        <div class="col-sm-2" style="padding-left:0;">
                          <button type="submit" name="edit" class="btn btn-success">Edit Info</button>
                        </div>
                      </div>
                  </form>
            </div>
            </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
          <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
       <? }  
      
       include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
    <script>
	$(".toggleimage").click(function () {
    $("#changepic").toggle();
     });
    </script>
    <script>
      $(function () {
        $('#crm_location').geocomplete();
      });
    </script>  
     <script type="text/javascript">
    $("#edituser").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>
  
  
  <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>
  
  </body>
</html>