<?php

function removeqsvar($url, $varname) {
    return preg_replace('/([?&])'.$varname.'=[^&]+(&|$)/','$1',$url);
}

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

//page id variable 
$pageid='manage-discoverables';
//core vars and logic
include'includes/application_top.php';	

if(isset($_REQUEST['admin']) &&$_REQUEST['admin']=='n' )  {
include ('includes/perms.php');	
$url=$_SERVER['PHP_SELF'];
removeqsvar($url,'admin');
}

//if form is submitted load processing script
if(isset($_POST['create']))  {
include_once ('includes/process_create_discoverable.php');	
}

if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='deactivate' )  {
	//process the deactivate
	include('includes/deactivate_discoverable.php');
}
if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='activate' )  {
	//process the activate
   include('includes/activate_discoverable.php');
}


//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;

//list of interests
$query_new = new ParseQuery("Interest");
$query_list_tags1 = $query_new->find();


//list of browsing tags
$query_new2 = new ParseQuery("browsingTags");
$query_list_tags2 = $query_new2->find();

//html header file 
include ('includes/header.php');

?>

<style>
.map_canvas3 {
height: 100%;
width:100%;	
}

.dataTables_wrapper .ui-toolbar{
    width: 50%;
}
</style>



<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Discoverables
          </h1>
          <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Manage Discoverables</li>
            
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">

            <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
				
               
               <?   if($_SESSION['admin_level']==false) {   ?> 
                  <li><a href="#your_discoverables" data-toggle="tab">Your Discoverables</a></li> 
             <?  } ?>
               
               <li class="active"><a href="#all_discoverables" data-toggle="tab"> All Discoverables   </a></li>
                   
                   
                   
                 <?  if($_SESSION['admin_level']==true) {   ?>  
                 <li><a href="#pending_discoverable" data-toggle="tab">Pending Discoverables</a></li> 
				 <?  } ?>
                 
                  <li><a href="#create_discoverable" data-toggle="tab">Create discoverable</a></li>
                 
                </ul>
           <?   if($_SESSION['admin_level']==false) {   ?>  
           <div class="tab-content">  
             
            <div class="tab-pane" id="your_discoverables">     
              <div class="box" style="border:none;">
                <div class="box-body">
                  <table id="example1" class="table">
                    <thead>
                      <tr>
                       <th style="width:10%;">Imagery</th>   
                        <th style="width:55%;">Bio</th>
                        <th style="width:15%;">Ambassador</th>
                        
                        <th style="width:5%;">Type</th>
                         <th style="width:5%;">City</th>
                         <th style="width:5%;">Status</th>
                        <th style="width:5%;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                  <? 

				
						//lets get the last contributor image string and the last jar id
						$query_discoverable_list = new ParseQuery("Discoverable");
						$query_discoverable_list->includeKey("contributor");
						
						$query_discoverable_list->descending("createdAt");
						
						if(isset($your_list) &&$your_list=='Y')  {
						
	 $query_discoverable_list->equalTo("contributor", array("__type" => "Pointer", "className" => "Contributor", "objectId" => $_SESSION['object_ref']));
							
						}
					
						//$query_discoverable_list->equalTo("inactive", false);
					    $query_discoverable_list->limit(500);
									        
                        try {
					     $list_discoverable_result = $query_discoverable_list->find();
					     if(count($list_discoverable_result)>0)  {		
                         foreach ($list_discoverable_result as $item ) {  
					     //lets build the vars 
	                     $imageName2=$item->get('imageName');
					     $title=$item->get('canonicalTitle');
					     $contact=$item->get('contact');
					     $detail=$item->get('detail');
					     $type=$item->get('type');	
					     $status=$item->get('inactive');
						 $jamjarCity=$item->get('jamjarCity');		
					     $object_ref=$item->getObjectId();	
					     $object_date=$item->getCreatedAt(); 
					     $created_at=date_format($object_date, 'd-m-Y');	      
					     $c_name=$item->get("contributor")->get('name'); 
					     $c_pic=$item->get("contributor")->get('imageName'); 
					
					   
					   if($status==true)  {
						  $inactive='INACTIVE';
						  $class_color2='label-danger';	
					    } 
					  
					   if($status==false)  {
						  $inactive='ACTIVE';
						  $class_color2='label-info';	
					   }
					   
					    if($jamjarCity=='Perth')  {
						
						  $class_color3='label-danger';	
					   }
					    if($jamjarCity=='Bali')  {
						
						  $class_color3='label-warning';	
					   }
	
					   if($type==1) { 
				
					   $typestring='PLACE';
					   $class_color='label-success';	
					   }   
					   if($type==2) {
					   $typestring='SPECIAL';
				       $class_color='label-warning';			
					   }
					   if($type==3) {
				       $typestring='EVENT';
					   $class_color='label-primary';			
					   }
								   	   
					   if (file_exists('../discoverable-images/256/'.$imageName2.'.jpg')) { $imagepath2='../discoverable-images/256/'.$imageName2.'.jpg';	    }  
					  
					   else if  (file_exists('../discoverable-images/256/'.$imageName2.'.png')) { $imagepath2='../discoverable-images/256/'.$imageName2.'.png';	    }  
					  
					  
					   else  { $imagepath2='dist/img/no_pic2.png'; }
					   
					      	   
					  if (file_exists('../ambassador-images/256/'.$c_pic.'.jpg')) { $imagepath3='../ambassador-images/256/'.$c_pic.'.jpg';	    }  
					      
					  else if   (file_exists('../ambassador-images/256/'.$c_pic.'.png')) { $imagepath3='../ambassador-images/256/'.$c_pic.'.png';	    }  
					   
					  else  { $imagepath3='dist/img/no_pic2.png'; }		
					  
					     	   
					   ?>
                        <tr>
                        <td><a href="edit-discoverable?ID=<?=$object_ref?>"><img class="img-responsive" src="<?=$imagepath2?>" alt="discoverable pic"></a></td>
                        <td style="width:50%;"><h4><?=$title?></h4><?=$detail?></td>                   
                        <td style="padding-top:50px;">  
                        <div class='user-block'>
                        <img class='img-circle' src='<?=$imagepath3?>' alt='user image'>
                        <span class='username'><a href="#"><?=$c_name?></a></span>
                        <span class='username' style="font-size:11px;"><a href="#">Submitted  <?=$created_at?></a></span>
                       
                        </div><!-- /.user-block -->  
                       </td>
                     
                        <td style="padding-top:50px;"> <span class="label <?=$class_color?>"><?=$typestring;?></span></td>
                         <td style="padding-top:50px;"> <span class="label <?=$class_color3?>"><?=$jamjarCity;?></span></td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color2?>"><?=$inactive;?></span></td>
                       
           
                        <td style="padding:30px;padding-top:50px;"><a href="edit-discoverable?ID=<?=$object_ref?>"  ><button class="btn btn-block btn-primary" style="padding:2px;"><span class="glyphicon glyphicon-cog"></span>&nbsp;Edit-Details</button></a>
                         <br>
                       <? if($status==true) { ?>
						<a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;" title="This will re-activate the discoverable"><button class="btn btn-block btn-success btn-success"  style="padding:2px;" data-id="<?=$object_ref?>" data-href="manage-discoverables?ID=<?=$object_ref?>&flag=activate" data-toggle="modal" data-target="#confirm-delete2"><span class="glyphicon glyphicon-ok"></span>&nbsp;Activate</button></a>
                         
                         <? } else if($status==false) { ?> 
                        <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;" title="This will de-activate the discoverable"><button class="btn btn-block btn-warning  btn-confirm"  style="padding:2px;" data-id="<?=$object_ref?>" data-href="manage-discoverables?ID=<?=$object_ref?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span>&nbsp;De-activate</button></a>
                         <?  }  ?>
                         </td>
                      </tr>   		   
                     <?  } }
					 
					 	} catch (ParseException $error) {
		  // $error is an instance of ParseException with details about the error.
		  echo $error->getCode();
		  echo "<br />";
		  echo $error->getMessage();
		}

					 
 ?>               
                  
                  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
           </div>
                  
    <?  }  ?>
                            
             <div class="active  tab-pane" id="all_discoverables">     
              <div class="box" style="border:none;">
                <div class="box-body">
                <table id="example3" class="table">
                    <thead>
                      <tr>
                      <th style="width:10%;">Imagery</th>   
                        <th style="width:55%;">Bio</th>
                        <th style="width:10%;">Ambassador</th>
                        <th style="width:5%;">Type</th>
                         <th style="width:5%;">City</th>
                         <th style="width:5%;">Status</th>
                        <th style="width:10%;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                  <? 

				
						//lets get the last contributor image string and the last jar id
						$query_discoverable_list3 = new ParseQuery("Discoverable");
						$query_discoverable_list3->includeKey("contributor");
						$query_discoverable_list3->notEqualTo("contributor", NULL);
						$query_discoverable_list3->descending("createdAt");
					    $query_discoverable_list3->limit(500);
									        
                        try {
					     $list_discoverable_result3=$query_discoverable_list3->find();
					     if(count($list_discoverable_result3)>0)  {		
                         foreach ($list_discoverable_result3 as $item3 ) {  
					     //lets build the vars 
	                     $imageName2=$item3->get('imageName');
					     $title=$item3->get('canonicalTitle');
					     $contact=$item3->get('contact');
					     $detail=$item3->get('detail');
					     $type=$item3->get('type');	
					     $status=$item3->get('inactive');
						 $jamjarCity=$item3->get('jamjarCity');		
					     $object_ref=$item3->getObjectId();	
					     $object_date=$item3->getCreatedAt(); 
					     $created_at=date_format($object_date, 'd-m-Y');	      
					     $c_name=$item3->get("contributor")->get('name'); 
					     $c_pic=$item3->get("contributor")->get('imageName'); 
					
					   
					   if($status==true)  {
						  $inactive='INACTIVE';
						  $class_color2='label-danger';	
					    } 
					  
		 
					    if($jamjarCity=='Perth')  {
						
						  $class_color3='label-danger';	
					   }
					    if($jamjarCity=='Bali')  {
						
						  $class_color3='label-warning';	
					   }
	
					   if($type==1) { 
				
					   $typestring='PLACE';
					   $class_color='label-success';	
					   }   
					   if($type==2) {
					   $typestring='SPECIAL';
				       $class_color='label-warning';			
					   }
					   if($type==3) {
				       $typestring='EVENT';
					   $class_color='label-primary';			
					   }
								   	   
					   if (file_exists('../discoverable-images/256/'.$imageName2.'.jpg')) { $imagepath2='../discoverable-images/256/'.$imageName2.'.jpg';	    }  
					  
					   else if  (file_exists('../discoverable-images/256/'.$imageName2.'.png')) { $imagepath2='../discoverable-images/256/'.$imageName2.'.png';	    }  
					  
					  
					   else  { $imagepath2='dist/img/no_pic2.png'; }
					   
					      	   
					  if (file_exists('../ambassador-images/256/'.$c_pic.'.jpg')) { $imagepath3='../ambassador-images/256/'.$c_pic.'.jpg';	    }  
					      
					  else if   (file_exists('../ambassador-images/256/'.$c_pic.'.png')) { $imagepath3='../ambassador-images/256/'.$c_pic.'.png';	    }  
					   
					  else  { $imagepath3='dist/img/no_pic2.png'; }		
					  
					     	   
					   ?>
                        <tr>
                        <td><a href="edit-discoverable?ID=<?=$object_ref?>"><img class="img-responsive" src="<?=$imagepath2?>" alt="discoverable pic"></a></td>
                        <td style="width:50%;"><h4><?=$title?></h4><?=$detail?></td>                   
                        <td style="padding-top:50px;">  <div class='user-block'>
                        <img class='img-circle' src='<?=$imagepath3?>' alt='user image'>
                        <span class='username'><a href="#"><?=$c_name?></a></span>
                     <!--  <span class='username' style="font-size:11px;">Submitted  <?=$created_at?> </span>-->

                        </div><!-- /.user-block -->  
                       </td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color?>"><?=$typestring;?></span></td>
                         <td style="padding-top:50px;"> <span class="label <?=$class_color3?>"><?=$jamjarCity;?></span></td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color2?>"><?=$inactive;?></span></td>
                       
           <?   if($_SESSION['admin_level']==false) {   ?>  
                        <td style="padding:30px;padding-top:50px;"><a href="edit-discoverable?ID=<?=$object_ref?>"  ><button class="btn btn-block btn-primary" style="padding:2px;"><span class="glyphicon glyphicon-eye"></span>&nbsp;view</button></a>
                         <br>
                     </td>
                     
                     <?  } else {  ?>
                     
                     
                     
                      <td style="padding:30px;padding-top:50px;"><a href="edit-discoverable?ID=<?=$object_ref?>"  ><button class="btn btn-block btn-primary" style="padding:2px;"><span class="glyphicon glyphicon-cog"></span>&nbsp;Edit Details</button></a>
                         <br>
                     </td>
                     
                     <?  }  ?>
                      </tr>   		   
                     <?  } }
					 
					 	} catch (ParseException $error) {
		  // $error is an instance of ParseException with details about the error.
		  echo $error->getCode();
		  echo "<br />";
		  echo $error->getMessage();
		}

					 
 ?>               
                  
                  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                   
       </div>
              
    
                   
                   
                  
             <?   if($_SESSION['admin_level']==true) {   ?>    
                                   
              <div class="tab-pane" id="pending_discoverable">     
              <div class="box" style="border:none;">
                <div class="box-body">
                <table id="example2" class="table">
                    <thead>
                      <tr>
                      <th style="width:10%;">Imagery</th>   
                        <th style="width:55%;">Bio</th>
                        <th style="width:10%;">Ambassador</th>
                        <th style="width:5%;">Type</th>
                         <th style="width:5%;">City</th>
                         <th style="width:5%;">Status</th>
                        <th style="width:10%;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                  <? 

				
						//lets get the last contributor image string and the last jar id
						$query_discoverable_list2 = new ParseQuery("Discoverable");
						$query_discoverable_list2->includeKey("contributor");
						$query_discoverable_list2->notEqualTo("contributor", NULL);
						$query_discoverable_list2->equalTo("publishStatus", 'Pending');
						$query_discoverable_list2->descending("createdAt");
					    $query_discoverable_list2->limit(500);
									        
                        try {
					     $list_discoverable_result2 = $query_discoverable_list2->find();
					     if(count($list_discoverable_result2)>0)  {		
                         foreach ($list_discoverable_result2 as $item2 ) {  
					     //lets build the vars 
	                     $imageName2=$item2->get('imageName');
					     $title=$item2->get('canonicalTitle');
					     $contact=$item2->get('contact');
					     $detail=$item2->get('detail');
					     $type=$item2->get('type');	
					     $status=$item2->get('inactive');
						 $jamjarCity=$item2->get('jamjarCity');		
					     $object_ref=$item2->getObjectId();	
					     $object_date=$item2->getCreatedAt(); 
					     $created_at=date_format($object_date, 'd-m-Y');	      
					     $c_name=$item2->get("contributor")->get('name'); 
					     $c_pic=$item2->get("contributor")->get('imageName'); 
					
					   
					   if($status==true)  {
						  $inactive='INACTIVE';
						  $class_color2='label-danger';	
					    } 
					  
		 
					    if($jamjarCity=='Perth')  {
						
						  $class_color3='label-danger';	
					   }
					    if($jamjarCity=='Bali')  {
						
						  $class_color3='label-warning';	
					   }
	
					   if($type==1) { 
				
					   $typestring='PLACE';
					   $class_color='label-success';	
					   }   
					   if($type==2) {
					   $typestring='SPECIAL';
				       $class_color='label-warning';			
					   }
					   if($type==3) {
				       $typestring='EVENT';
					   $class_color='label-primary';			
					   }
								   	   
					   if (file_exists('../discoverable-images/256/'.$imageName2.'.jpg')) { $imagepath2='../discoverable-images/256/'.$imageName2.'.jpg';	    }  
					  
					   else if  (file_exists('../discoverable-images/256/'.$imageName2.'.png')) { $imagepath2='../discoverable-images/256/'.$imageName2.'.png';	    }  
					  
					  
					   else  { $imagepath2='dist/img/no_pic2.png'; }
					   
					      	   
					  if (file_exists('../ambassador-images/256/'.$c_pic.'.jpg')) { $imagepath3='../ambassador-images/256/'.$c_pic.'.jpg';	    }  
					      
					  else if   (file_exists('../ambassador-images/256/'.$c_pic.'.png')) { $imagepath3='../ambassador-images/256/'.$c_pic.'.png';	    }  
					   
					  else  { $imagepath3='dist/img/no_pic2.png'; }		
					  
					     	   
					   ?>
                        <tr>
                        <td><a href="edit-discoverable?ID=<?=$object_ref?>"><img class="img-responsive" src="<?=$imagepath2?>" alt="discoverable pic"></a></td>
                        <td style="width:50%;"><h4><?=$title?></h4><?=$detail?></td>                   
                        <td style="padding-top:50px;">  <div class='user-block'>
                        <img class='img-circle' src='<?=$imagepath3?>' alt='user image'>
                        <span class='username'><a href="#"><?=$c_name?></a></span>
                     <!--  <span class='username' style="font-size:11px;">Submitted  <?=$created_at?> </span>-->

                        </div><!-- /.user-block -->  
                       </td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color?>"><?=$typestring;?></span></td>
                         <td style="padding-top:50px;"> <span class="label <?=$class_color3?>"><?=$jamjarCity;?></span></td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color2?>"><?=$inactive;?></span></td>
                       
           
                        <td style="padding:30px;padding-top:50px;"><a href="edit-pending-discoverable?ID=<?=$object_ref?>"  ><button class="btn btn-block btn-primary" style="padding:2px;"><span class="glyphicon glyphicon-cog"></span>&nbsp;Edit-Details</button></a>
                         <br>
                     </td>
                      </tr>   		   
                     <?  } }
					 
					 	} catch (ParseException $error) {
		  // $error is an instance of ParseException with details about the error.
		  echo $error->getCode();
		  echo "<br />";
		  echo $error->getMessage();
		}

					 
 ?>               
                  
                  
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                   
              </div>
              
              <?  } ?>
         
                  
                   <div class="tab-pane" id="create_discoverable">
                    <div class="box" style="border:none;">
                   <div class="box-header">
                    <h3 class="box-title">Add a title and Sub title and Type</h3>
                  </div><!-- /.box-header -->
                <div class="box-body">
                     <form enctype="multipart/form-data" method="post" id="creatediscoverable" action="manage-discoverables#create_discoverable">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Discoverable Title</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Enter an approriate title   (No more than 100 characters)..."  data-msg-required="A title is required." data-rule-required="true" >
                          <p class="help-block" id="chars2">100</p>
                    </div>
             
                       <div class="form-group">
                       <label>Select an approriate type</label>
                     <select class="form-control" name="type"  id="type" data-msg-required="A type of discoverable is required." data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <option value="1">Place</option>
                        <option value="2">Special</option>
                        <option value="3">Event</option>                      
                      </select>
                   </div>
                  
                 
                       <div class="form-group" style="display:none;" id="subtitleblock">
                      <label>Sub Title</label>
                      <input type="text" class="form-control" id="sub_title" name="sub_title"  placeholder="Enter a suitable sub-title... (No more than 100 characters)">
                       <p class="help-block" id="chars3">100</p>
                       
                      
                    </div>
                   <? if(isset($_REQUEST['admin']) &&$_REQUEST['admin']=='n' )  {   ?>
                        <input type="hidden" name="admin" value="n">
                  <?  }  ?>
                  
                  
                  
                  <div id="choose_dates" style="display:none;">
                      
                   <div class="box-header" style="padding-left:0; padding-top:20px;">
                   <h3 class="box-title">Give the Timeslot a label, an expiry date, and select specific dates from the start /  end date calender below </h3>
                   </div><!-- /.box-header -->
                  
                    <div class="form-group">
                    <label>Input a label for the times(s) slot </label>
                      <input type="text" name="timeslotstring" class="form-control" placeholder="Eg. Every Monday And Wednesday @ 8.30-10.30 pm" id="timestring" 
                      data-msg-required="a label for the timeslot is required." data-rule-required="true" autofocus>
                    </div><!-- /.input group -->
                 
                    <div class="form-group">
                    <label>Expiry date</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteexpiry" class="form-control pull-right timeslotsingle" placeholder="please choose an expiry date">
                    </div><!-- /.input group -->                
                  </div>
                  
                    <div class="form-group">
                    <label>start  / end </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="dteTime" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" id="date" data-msg-required="dates are required." data-rule-required="true">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  
                  <div class="form-group" id="parentsection">   
                  
                    <button type="button" class="btn btn-primary" onClick="addSlot();">add new time slot</button> 
                   </div> 
                  
                     <div class="form-group" style="display:none;" id="first">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_1" class="btn btn-primary" onClick="addSlot1();">add new time slot</button>
                  </div>
                 
                     
                      <div class="form-group" style="display:none;" id="second">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_2"  class="btn btn-primary" onClick="addSlot2();">add new time slot</button>
                  </div>
                 
                 
                         
                     <div class="form-group" style="display:none;" id="third">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_3"  class="btn btn-primary" onClick="addSlot3();">add new time slot</button>
                  </div>
                 
                      <div class="form-group" style="display:none;" id="fourth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_4"  class="btn btn-primary" onClick="addSlot4();">add new time slot</button>
                  </div>    
                  
                     <div class="form-group" style="display:none;" id="fifth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_5"  class="btn btn-primary" onClick="addSlot5();">add new time slot</button>
                  </div>  
                  
                     <div class="form-group" style="display:none;" id="sixth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_6"  class="btn btn-primary" onClick="addSlot6();">add new time slot</button>
                  </div>   
                  
                    <div class="form-group" style="display:none;" id="seven">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_7"  class="btn btn-primary" onClick="addSlot7();">add new time slot</button>
                  </div>     
                    
                    <div class="form-group" style="display:none;" id="eight">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>    
                  </div>  
                  </div>   
              
                   <div class="form-group">
                      <label for="exampleInputFile">Upload an Image / profile pic </label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                    </div>              
                    <!-- checkbox -->   
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title">Description of discoverable</h3>
                </div><!-- /.box-header -->

                    <div class="form-group">         
                      <textarea class="form-control" id="desc" name="discoverable_desc" rows="5" placeholder="Enter a description here  (No more than 2000 characters)..." data-msg-required="A description of the discoverable is required." data-rule-required="true"></textarea>
                      <p class="help-block" id="chars">2000</p>
                    </div>

                 
                     <div class="box-header" style="padding-left:0;">
                     <h3 class="box-title">More details</h3>
                     </div>
        
                    <div class="form-group">
                      <label>Address </label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="begin typing to auto complete the location" 
                      data-msg-required="An address is required." data-rule-required="true">
                    </div>
                    <div class="form-group">
                      <label>Location (lattitide and Longitude)</label>
                    </div>
                    
                    <div class="col-xs-3" style="padding-left:0;">
                      <input type="text" class="form-control" name="lat" id="lat" placeholder="latitude.." value="">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" name="lng" id="lng"  placeholder="longitude..." value="">
                    </div>
                    
                    
                           
                   <br style="clear:both"><br>
                   
                       <div class="form-group">
                       <label>Jam Jar City</label>
                     <select class="form-control" name="jamjarcity"  id="jamjarcity" data-msg-required="A city is required" data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <option value="Bali">Bali</option>
                        <option value="Perth">Perth</option>
                                       
                      </select>
                   </div>
                  
                    
                   
               
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Price</h3>
                   </div><!-- /.box-header -->
                    <div class="form-group">
                      <input type="text" class="form-control" name="cost" id="cost" placeholder="how much">
                    </div>
                    
                       
                      <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Contact Number</h3>
                   </div><!-- /.box-header -->
                    
                     <div class="form-group">
                      <input type="text" class="form-control" name="contact" id="contact" placeholder="contact number">
                    </div>
           
                    <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tips</h3>
                   </div><!-- /.box-header -->  
                    
                     <ul id="list2">
                    <li class="default" style="display: none; margin-bottom:10px; list-style-type:none;">
                        <input placeholder="new tip" type="text" name="tips[]"/><span style="cursor: pointer;" onClick="closeMe2(this);"> <i class="fa fa-close"></i></span>
                    </li>
                    </ul>
                            
                    <div class="form-group">   
                    <button type="button" class="btn btn-primary" onClick="addTip();">add a tip</button>
                    </div> 

                  <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tags / interests</h3>
                   </div><!-- /.box-header -->
                    <?
					 $counter=0; 
                     if(count($query_list_tags1)>0)  {		
                      foreach ($query_list_tags1 as $item1 ) {
					  if (($counter + 1) % 6 == 0) {  echo '<br><br><br>'; }			   
					  $objectRef= $item1->getObjectId();   ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon" >
                          <input type="checkbox" name="tags[]" value="<?=$objectRef?>">
                        </span>
                       <a id="popover" data-hover="tooltip"  title="<?=$item1->get('title')?>">    <input type="text" class="form-control" placeholder="<?=$item1->get('title')?>" readonly></a>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->   
                                    
                    <? $counter++;  } } ?>
                     <br style="clear:both"><br>
                    <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Browsing Tags</h3>
                   </div><!-- /.box-header -->
                 
                   <?
				     $counter=0;
                     if(count($query_list_tags2)>0)  {		
                     foreach ($query_list_tags2 as $item2 ) { 
				     if (($counter + 1) % 6 == 0) {  echo '<br><br><br>'; }		
					 $objectRef2= $item2->getObjectId();  ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox"  name="tags2[]" value="<?=$objectRef2?>">
                        </span>
                       <a id="popover" data-hover="tooltip"  title="<?=$item2->get('title')?>">  
                        <input type="text" class="form-control" placeholder="<?=$item2->get('title')?>" readonly></a>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 --> 
                    <? $counter++; } } ?>
                      <br style="clear:both"><br>
                      <div class="form-group">
                        <br>
                          <button type="submit" name="create" class="btn btn-success btn-lrg">Create discoverable</button>
                       
                        </div>
                     </form>  
                  </div><!-- /.box -->    
                    
                 
                </div><!-- /.tab-content -->           
                
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->         
 
           
      
        <div class="example-modal5" style="display:none;" id="confirm-delete">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will de-activate the discoverable</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">De-activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
            <div class="example-modal6" style="display:none;" id="confirm-delete2">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will re-activate the discoverable</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
      
       <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
          
          
        <? }  
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
  

<script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Discoverable ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    
     <script>
	$('#confirm-delete2').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Discoverable ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
  

    <script type="text/javascript">
$(document).ready(function(){
    $('[data-hover="tooltip"]').tooltip({
      
    });
});
</script>


 <script type="text/javascript">
    $("#creatediscoverable").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>

      <script>
      $(function () {
        $("#example1").DataTable({  stateSave: true,  "iDisplayLength": 100 ,  
		 "aoColumnDefs": [ {
			                  "ordering": false,
                            'bSortable': false,
                            'aTargets': [0,1,2,6]
                        } //disables sorting for column one
            ], "aLengthMenu": [5, 10, 25, 50, 100]});
         $('.dataTables_filter input').attr("placeholder", "Search by any of the displayed columns here to sort the list");
		   $('.dataTables_filter input').css("width", "350px");

       
      });
    </script>
    
      <script>
      $(function () {
        $("#example2").DataTable({ stateSave: true, "iDisplayLength": 100 , 
		
		 "aoColumnDefs": [ {
			                  "ordering": false,
                            'bSortable': false,
                            'aTargets': [0,1,5,6]
                        } //disables sorting for column one
            ], "aLengthMenu": [5, 10, 25, 50, 100]});
			
         $('.dataTables_filter input').attr("placeholder", "Search by any of the displayed columns here to sort the list");
		   $('.dataTables_filter input').css("width", "350px");

      
      });
    </script>
    
      <script>
      $(function () {
        $("#example3").DataTable({ stateSave: true, "iDisplayLength": 100 , 
		
		 "aoColumnDefs": [ {
			                  "ordering": false,
                            'bSortable': false,
                            'aTargets': [0,1,5,6]
                        } //disables sorting for column one
            ], "aLengthMenu": [5, 10, 25, 50, 100]});
			
         $('.dataTables_filter input').attr("placeholder", "Search by any of the displayed columns here to sort the list");
		   $('.dataTables_filter input').css("width", "350px");

      
      });
    </script>
    
    <script>
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

</script>  
<script>
var elem = $("#chars2");
$("#title").limiter(100, elem);
</script>

<script>
var elem = $("#chars3");
$("#sub_title").limiter(100, elem);
</script>
     <script>
var elem = $("#chars");
$("#desc").limiter(2000, elem);
</script>
      <script>
      $(function () {		  
		  var mapoptions = {
		  details: "form",
		  types: ['geocode','establishment']
        };
        
		$('#address').geocomplete(mapoptions)		
       
});

  </script> 
  
  
 <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
 <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
  
<?  } ?>
  
  
    
<script>

function closeMe2(element) {
  $(element).parent().remove();
}

function addTip() {
  var container = $('#list2');
  var item = container.find('.default').clone();
  item.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item.appendTo(container).show();
}


function addSlot() {
  $('#parentsection').hide();
  $('#first').show();
  
}

function addSlot1() {

 $('#button_1').hide();
  $('#second').show();
  
}


function addSlot2() {
 $('#button_2').hide();
  $('#third').show();
  
}

function addSlot3() {
 $('#button_3').hide();
  $('#fourth').show();
  
}

function addSlot4() {
 $('#button_4').hide();
  $('#fifth').show();
  
}

function addSlot5() {
 $('#button_5').hide();
  $('#sixth').show();
  
}

function addSlot6() {
 $('#button_6').hide();
  $('#seven').show();
  
}

function addSlot7() {
 $('#button_7').hide();
  $('#eight').show();
  
}


</script>  


 <script>
 // Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})
</script>

<script>
  $(function () {
    $('.datetimeslot').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY H:mm'});
   $('.timeslotsingle').daterangepicker({timePicker: true, singleDatePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY H:mm'});
   })
</script>  
    
   
<script type="text/javascript">
$(document).ready(function(){
    $("#type").change(function(){
		
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")==2){
                
                $("#choose_dates").show();
            }
            else if($(this).attr("value")==3){
              
                $("#choose_dates").show();
            }
           
            else{
                $("#choose_dates").hide();
				
				 $("#subtitleblock").show();
            }
        });
    })
});
</script>

   <script>
      $(function () {		  
		  var mapoptions = {
		  details: "form",
		  types: ['(cities)']
        };
        
		$('#address').geocomplete(mapoptions)		
       
});

  </script> 

 
  
  </body>
</html>
