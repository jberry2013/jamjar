<?php
//page id variable 
$pageid='manage-ambassadors';
//core vars and logic
include'includes/application_top.php';	
//html header file 
include ('includes/header.php');

//if form is submitted load processing script
if(isset($_POST['edit']))  {
include ('includes/process_edit_ambassador.php');	
}

if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='deactivate' )  {
	//process the deactivate
	include('includes/deactivate.php');
}
if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='activate' )  {
	//process the activate
   include('includes/activate.php');
}


//list of ambassadors
include ('includes/ambassadors_profile.php');	

?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
  <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
        Manage Ambassador Profile 
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
               <li><a href="manage-ambassadors"><i class="fa fa-users"></i> Manage Ambassadors</a></li>
            <li class="active">Edit Ambassador</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?=$imagepath2?>" alt="User profile picture">
                  <h3 class="profile-username text-center"><?=$am_name?></h3>
                  <p class="text-muted text-center">Some Title</p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>How Many Discoverables</b> <a class="pull-right"><?=$cnt_discoverables?></a>
                    </li>
                   
                    <li class="list-group-item"> 
                      <b>Total Discoverable Likes</b> <a class="pull-right">0</a>
                    </li>
                  </ul>

               
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Actions</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                
                <div class="form-group">
                
                <?=$imgsource;?>
                
                  
             <? if ($inactive==false)  {  ?>
                  <div class="col-sm-4">
                  <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;" title="This will suspend the ambassador">
                  <button class="btn   btn-warning" style="padding: 2px;" data-id="<?=$object_ref?>" data-href="edit-ambassador?ID=<?=$object_ref?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete2"><span class="glyphicon glyphicon-pause"></span>De-activate</button></a>
                  </a>
                  </div>
				  
				  <?  } 
				  else if ($inactive==true) { ?>
                   <div class="col-sm-4">
                   <a id="popover" data-toggle="tooltip" data-placement="top"    style="cursor:pointer;" title="This will re-activate the ambassador ">   
                   <button class="btn  btn-success" style="padding:2px;" data-id="<?=$object_ref?>" data-href="edit-ambassador?ID=<?=$object_ref?>&flag=activate" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-ok"></span> Activate</button></a>
                   </a>
                  </div> 
                  <?  }  ?>          
               </div>
               
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
               <div class="box box-primary">
            
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Ambassador</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
               
                     <form role="form" id="edituser"  enctype="multipart/form-data" method="post" action="edit-ambassador">
                    <!-- text input -->
                    <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                       <input type="text" class="form-control" placeholder="name of ambassador" name="am_name"  value="<? if(isset($am_name)) echo $am_name ?>">
                       <input type="hidden" class="form-control" placeholder="name of ambassador" name="ID" id="ID"  value="<? if(isset($object_ref)) echo $object_ref ?>">
                        <input type="hidden" class="form-control" placeholder="name of ambassador" name="am_image" id="am_image"  value="<? if(isset($am_image)) echo $am_image ?>">
                       
                    </div>
                    <br>
                    
                    <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                       <input type="text" class="form-control" placeholder="email of ambassador"  name="am_email"  value="<? if(isset($am_email)) echo $am_email ?>">
                    </div>
                    <br>
                       <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                       <input type="text" class="form-control" placeholder="location of ambassador" name="am_location" id="crm_location"  value="<? if(isset($am_location)) echo $am_location ?>">
                    </div>
                    <br>
                     <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                       <input type="text" class="form-control" placeholder="URL Attached to ambassador " name="am_url"  value="<? if(isset($am_url)) echo $am_url ?>">
                    </div>
                    <br>
                      <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                       <input type="text" class="form-control" placeholder="Change the password" name="password"  value="<? if(isset($am_password)) echo $am_password ?>">
                    </div>
                    
                    <br>
                   <div class="form-group" style="float:left;">
                      <label for="exampleInputFile">CRM Access Level </label>
                       <p class="help-block">if the box is checked it means this user has admin rights , uncheck the box to change</p>
               <div class="checkbox icheck">
                <label>
                  <input type="checkbox"  name="access" value="yes" id="admin" class="role" <? if(isset($admin_level) && $admin_level==true) echo "checked='checked'"?>>&nbsp; Admin level access
                </label>
              </div>
              
              
                    </div> <br style="clear:both;">
                   
                    
                  
                    
                <div class="box-header" style="margin-left:0px; padding-left:0px;">
                  <h3 class="box-title"> <a href="#" class="toggleimage" style="color:#333;"><i class="fa fa-picture-o"></i> Upload a new Image </a></h3>
                </div><!-- /.box-header -->
                
                  
             
                  
                    
                   
                    
                   <br style="clear:both">
                   <div class="form-group" id="changepic" style="display:none;">
                  <div class="col-xs-4" style="margin-left:0px; padding-left:0px;"> 
     
                     <label for="exampleInputFile">Browse for image</label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                      </div>
                    </div>               
                   <br style="clear:both;">
                    
                         
               
                
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title">Edit the bio / description of the ambassador </h3>
                </div><!-- /.box-header -->

                      <div class="form-group">
                     
                      <textarea class="form-control" name="am_bio" rows="5" placeholder="Enter a description here  (No more than 100 words)..."><?=$am_bio?></textarea>
                    </div>
                    
                     <div class="form-group">
                      <div class="col-sm-2" style="padding-left:0;">
                      <button type="submit" name="edit" class="btn btn-success">Edit Info</button></a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
       <div class="example-modal5" style="display:none;" id="confirm-delete2">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will de-activate the ambassador / user</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning btn-ok">De-activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->  
         <div class="example-modal6" style="display:none;" id="confirm-delete">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will re-activate the ambassador / user</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-success btn-ok">Activate</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
      
      <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
       <? }  
      
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
   
     <script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Ambassador ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    
     <script>
	$('#confirm-delete2').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Ambassador ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    
   
    <script>
	$(".toggleimage").click(function () {
    $("#changepic").toggle();
     });
    </script>
    
    <script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>
<script>
      $(function () {
        $('#crm_location').geocomplete();
      });
    </script>  
     <script type="text/javascript">
    $("#edituser").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>
 <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    
    <script>
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

</script>  
<script>
var elem = $("#chars");
$("#crm_bio").limiter(500, elem);
</script>


  <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>
    

  </body>
</html>
