<?php
//page id variable 
$pageid='dashboard';
//core vars and logic
include'includes/application_top.php';	
//html header file 
include ('includes/header.php');

include ('includes/dashboard_parse_queries.php');
?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
           <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-user-plus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Mates</span>
                  <span class="info-box-number"><?=$cnt_mates?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-thumbs-o-up"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Likes</span>
                  <span class="info-box-number"><?=$cnt_likes?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-diamond"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Discoverables</span>
                  <span class="info-box-number"><?=$cnt_discoverable?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Contributors</span>
                  <span class="info-box-number"><?=$cnt_contributor?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">CRM Shortcuts</h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      </div>
                </div>
                <div class="box-body">
                  <? if($_SESSION['admin_level']==false) { ?>
                  <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-discoverables?admin=n">
                    <i class="fa fa-eye"></i>View Discoverables
                  </a>
                    <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-discoverables?admin=n#create_discoverable"> 
                    <i class="fa fa-building-o"></i> Create Discoverable
                  </a>
                  
                  <?  } else { ?>
                  
                   <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-discoverables">
                    <i class="fa fa-eye"></i>View Discoverables
                  </a>
                  
                     <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-discoverables#create_discoverable"> 
                    <i class="fa fa-building-o"></i> Create Discoverable
                  </a>
                  
                  <?  }  ?>
                  
               
                  <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-profile">
                    <i class="fa fa-user"></i> Manage Profile
                  </a>
                  <? if($_SESSION['admin_level']==true) { ?>
                  <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-ambassadors">
                    <i class="fa fa-users"></i>  Manage Ambassadors
                  </a>
                  
                  <a class="btn btn-app" href="http://www.jamjarapp.com.au/admin-crm/manage-beacons">
                    <i class="fa fa fa-cogs"></i> Manage Beacons
                  </a>
                  
                  <?  }  ?>
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <!-- Chat box -->
               <div class="box box-danger">
                    <div class="box-header with-border">
                      <h3 class="box-title">Latest Ambassadors</h3>
                      <div class="box-tools pull-right">
                        <span class="label label-danger">8 Newest </span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    
                 <ul class="users-list clearfix">
                     <?
                      if(count($results_contributors)>0)  {   
				        foreach ($results_contributors as $item ) { 
						 if (file_exists('../ambassador-images/256/'.$item->get('imageName').'.jpg')) { $imagepath='../ambassador-images/256/'.$item->get('imageName').'.jpg';	    }  
					      else  { $imagepath='dist/img/no_pic2.png'; }	 	  
							 $object_date=$item->getCreatedAt(); 
						     $object_ref_latest=$item->getObjectId();	
							 $profilelink='edit-ambassador?ID='.$object_ref_latest;
					         $created_at=date_format($object_date, 'd-m-Y');	       
						
						?>   
                        <? if($_SESSION['admin_level']==true) { ?>
                         <li>
                          <img src="<?=$imagepath?>" alt="User Image"  class="img-responsive">
                          
                          <a class="users-list-name" href="<?=$profilelink?>"><?=$item->get('name');?></a>
                          <span class="users-list-date"><?=$created_at?></span>
                        </li>
                        <?  }  else { ?>                      
                        <li>
                          <img src="<?=$$query5?>" alt="User Image"  class="img-responsive">                          
                          <span class="users-list-date"><?=$created_at?></span>
                        </li>                
                     <? } } } ?>        
                      </ul>
                 <!-- /.users-list -->   
                    
                    
                    
                      <!--     <div class="column dt-sc-one-third first">
                        <div class="dt-sc-team type3">
                            <div class="image">
                                <img src="images/team-img5.jpg" alt="" title="">                                
                            </div>
                            <div class="team-details">
                                <h4><a href="#"> Scorlett Johanson </a></h4>
                                <h5>Professional Hairstylist</h5>
                                <div class="hr-separator type2"></div>
                                <div class="hr-invisible-very-small"></div>
                                <p>
                                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi lectus, tincidunt ac mi id, iaculis porta tortor.
                                </p>
                                <div class="team-details-social-icons">
                                    <a class="fa fa-facebook" href="#"> </a>
                                    <a class="fa fa-twitter" href="#"> </a>
                                    <a class="fa fa-google-plus" href="#"> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column dt-sc-one-third">
                        <div class="dt-sc-team type3">
                            <div class="image">
                                <img src="images/team-img7.jpg" alt="" title="">                                
                            </div>
                            <div class="team-details">
                                <h4><a href="#"> Mark Vick </a></h4>
                                <h5>Professional Hairstylist</h5>
                                <div class="hr-separator type2"></div>
                                <div class="hr-invisible-very-small"></div>
                                <p>
                                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi lectus, tincidunt ac mi id, iaculis porta tortor.
                                </p>
                                <div class="team-details-social-icons">
                                    <a class="fa fa-facebook" href="#"> </a>
                                    <a class="fa fa-twitter" href="#"> </a>
                                    <a class="fa fa-google-plus" href="#"> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column dt-sc-one-third">
                        <div class="dt-sc-team type3">
                            <div class="image">
                                <img src="images/team-img6.jpg" alt="" title="">                                
                            </div>
                            <div class="team-details">
                                <h4><a href="#"> Angel Sienna </a></h4>
                                <h5>Professional Hairstylist</h5>
                                <div class="hr-separator type2"></div>
                                <div class="hr-invisible-very-small"></div>
                                <p>
                                	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mi lectus, tincidunt ac mi id, iaculis porta tortor.
                                </p>
                                <div class="team-details-social-icons">
                                    <a class="fa fa-facebook" href="#"> </a>
                                    <a class="fa fa-twitter" href="#"> </a>
                                    <a class="fa fa-google-plus" href="#"> </a>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    
                    
                    
                    
                    
                   
                    </div><!-- /.box-body -->
                     <? if($_SESSION['admin_level']==true) { ?>
                    <div class="box-footer text-center">                
                      <a href="manage-ambassadors" class="uppercase">View All Ambassadors</a>                 
                    </div><!-- /.box-footer -->
                    <?  }  ?>
                  </div><!--/.box -->
            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

               <div class="box box-danger">
                <div class='box-header with-border'>  
                <h3 class="box-title">Latest Discoverable</h3>   
                 <div class="box-tools pull-right">
                 <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                 </div>
                 </div>     
                <div class='box-body'>
                 <? if($_SESSION['admin_level']==true) { ?>
                  <a  href="<?=$discoverable_url?>"><img class="img-responsive pad" src="<?=$imagepath_discover?>" alt="Photo"> </a>                
        <h4 class="pad"><a  href="<?=$discoverable_url?>" > <?=$last_discoverable_title?></a>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="label <?=$class_color?>"><?=$typestring;?></span></h4><br>
        <?  }  else { echo'<img class="img-responsive pad" src="'.$imagepath_discover.'" alt="Photo"> </a>                
        <h4 class="pad"> '.$last_discoverable_title.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="label <?=$class_color?>"><?=$typestring;?></span></h4><br>';   } ?>
                  <p class="pad">  <?=$last_discoverable_bio?></p>                   
                    <div class='user-block  pad'>
                    <img class='img-circle' src='<?=$imagepath_discover_amb?>' alt='user image'>
                    <span class='username'><a href="#"><?=$c_name_discover?></a></span>
                    <span class='description'> <?=$discoverable_created_at?></span>
                  </div><!-- /.user-block -->                   
                </div><!-- /.box-body -->              
              </div><!-- /.box -->
            </section><!-- right col -->
          </div><!-- /.row (main row) -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
       <? 
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
  </body>
</html>
