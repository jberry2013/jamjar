<? 


						//include the use of teh classes in this script
						use Parse\ParseClient;
						use Parse\ParseObject;
						use Parse\ParseQuery;
						//lets get the last contributor image string and the last jar id
						$query_discoverable_list = new ParseQuery("Discoverable");
						$query_discoverable_list->includeKey("contributor");
						$query_discoverable_list->notEqualTo("contributor", NULL);
						//$query_discoverable_list->equalTo("inactive", false);
					    $query_discoverable_list->limit(500);
									  
                     
                        try {
					     $list_discoverable_result = $query_discoverable_list->find();
					     if(count($list_discoverable_result)>0)  {		
                         foreach ($list_discoverable_result as $item ) {  
					   //lets build the vars 
	                   $imageName2=$item->get('imageName');
					   $title=$item->get('canonicalTitle');
					   $contact=$item->get('contact');
					   $detail=$item->get('detail');
					   $type=$item->get('type');	
					   $status=$item->get('inactive');		
					   $object_ref=$item->getObjectId();	
					   $object_date=$item->getCreatedAt(); 	      
					   $c_name=$item->get("contributor")->get('name'); 
					   $c_pic=$item->get("contributor")->get('imageName'); 
					
					  // $c_pic=$item->get("contributor")->get('imageName');    
					  
					  if($status==true)  {
						  $inactive='INACTIVE';
						  $class_color2='label-danger';	
					  }
					  
					  if($status==false)  {
						  $inactive='ACTIVE';
						  $class_color2='label-info';	
					  }
			
			
					
					  
					   if($type==1) { 
				
					   $typestring='PLACE';
					   $class_color='label-success';	
					   }   
					   if($type==2) {
					   $typestring='SPECIAL';
				       $class_color='label-warning';			
					   }
					   if($type==3) {
				       $typestring='EVENT';
					   $class_color='label-primary';			
					   }
								   	   
					   if (file_exists('../jamjar/img/discoverables/'.$imageName2.'.jpg')) { $imagepath2='../jamjar/img/discoverables/'.$imageName2.'.jpg';	    }  
					   else  { $imagepath2='dist/img/no_pic2.png'; }	
					   
					   
					   if (file_exists('../uploads/128/'.$c_pic.'.jpg')) { $imagepath3='../uploads/128/'.$c_pic.'.jpg';	    }  
					   else  { $imagepath3='dist/img/no_pic2.png'; }		   	   
					   ?>
                        <tr>
                        <td><a href="edit-discoverable?ID=<?=$object_ref?>"><img class="img-responsive" src="<?=$imagepath2?>" alt="discoverable pic" style="width:128px; height:128px;" ></a></td>
                        <td style="width:50%;"><h4><?=$title?></h4><?=$detail?></td>                   
                        <td style="padding-top:50px;">  <div class='user-block'>
                        <img class='img-circle' src='<?=$imagepath3?>' alt='user image'>
                        <span class='username'><a href="#"><?=$c_name?></a></span>
                        <span class='description'>Submitted  <?=$object_date?></span>
                        </div><!-- /.user-block -->  
                       </td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color?>"><?=$typestring;?></span></td>
                        <td style="padding-top:50px;"> <span class="label <?=$class_color2?>"><?=$inactive;?></span></td>
           
                        <td style="padding:30px;padding-top:50px;"><a href="edit-ambassador?ID=<?=$object_ref?>"  ><button class="btn btn-block btn-primary" style="padding:2px;"><span class="glyphicon glyphicon-cog"></span>&nbsp;Edit</button></a>
                        <br><button class="btn btn-block btn-danger warning btn-confirm"  style="padding:2px;" data-id="<?=$object_ref?>" data-href="manage-ambassadors?ID=<?=$object_ref?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span>&nbsp;De-activate</button></td>    
                      </tr>   		   
                     <?  } }
					 
					 	} catch (ParseException $error) {
		  // $error is an instance of ParseException with details about the error.
		  echo $error->getCode();
		  echo "<br />";
		  echo $error->getMessage();
		}

					 
					 
					  ?>               


			


