<?php
//page id variable 
$pageid='manage-beacons';
//core vars and logic
include ('includes/application_top.php');	

//html header file 
include ('includes/header.php'); 


//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;

//list of intesrests
$query_new = new ParseQuery("Interest");
$query_list_tags1 = $query_new->find();


//list of browsing tags
$query_new2 = new ParseQuery("browsingTags");
$query_list_tags2 = $query_new2->find();


if(isset($_POST['add']))  {
include ('includes/process_add_tags.php');	
}

if(isset($_GET['delete1']))  {
include ('includes/process_delete_tags.php');	
}

if(isset($_GET['delete2']))  {
include ('includes/process_delete_tags2.php');	
}

?>     

<style>

.ln_solid {
    border-top: 1px solid #e5e5e5;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 20px 0;
}

/* padding-bottom and top for image */
.mfp-no-margins img.mfp-img {
	padding: 0;
}
/* position of shadow behind the image */
.mfp-no-margins .mfp-figure:after {
	top: 0;
	bottom: 0;
}
/* padding for main container */
.mfp-no-margins .mfp-container {
	padding: 0;
}


</style>

  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Tags
      
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
           
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Tags</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
 				  <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                   <li class="active"><a href="#all_ambassadors" data-toggle="tab">Add Tags</a></li>
                  <li><a href="#create_ambassador" data-toggle="tab">Edit / Delete tags </a></li>
                </ul>
           <div class="tab-content">      
            <div class="active tab-pane" id="all_ambassadors">     
             
                <div class="box-body">
                                   
                                                    <form id="createuser" method="POST" action="manage-tags" enctype="multipart/form-data">
            
               <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Add New Interest Tags</h3>
                   </div><!-- /.box-header -->
            
                <div class="form-group">   
                    <button type="button" class="btn btn-primary" onClick="addTag();">add interest tag</button>
                    </div> 
                    
                       <ul id="list1" style="margin-left:0px;padding-left:0px; float:left;">
                    <li class="default" style="display: none; margin-bottom:10px; list-style-type:none;">
                        <input placeholder="interest tag title" style="width:180px; padding:3px;" type="text" name="itags[]"/>
                    </li>
                    </ul>
                    
                     <ul id="list1b"  style="margin-left:0px; float:left; margin-left:10px;padding-left:0px;">
                    <li class="default2" style="display: none; margin-bottom:10px; margin-top:5px;list-style-type:none;">
                      <input type="file" name="interest_img[]"/>
                    </li>
                    </ul>
            <br style="clear:both;">
                 <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Add New Browsing Tags</h3>
                   </div>
                   
                      <div class="form-group">   
                    <button type="button" class="btn btn-primary" onClick="addTag2();">add browsing tag</button>
                    </div> 
                    
                         <ul id="list2"  style="margin-left:0px;padding-left:0px;float:left;">
                    <li class="default" style="display: none; margin-bottom:10px; list-style-type:none;">
                        <input placeholder="browsing tag title" style="width:180px; padding:3px;" type="text" name="btags[]"/>
                    </li>
                    </ul>
                    
                     <ul id="list2b"  style="margin-left:0px; float:left; margin-left:10px;padding-left:0px;">
                    <li class="default2b" style="display: none; margin-bottom:10px; margin-top:5px;list-style-type:none;">
               <input type="file" name="browsing_img[]"/>
                    </li>
                    </ul>
                       <br style="clear:both;"> 
                     <div class="ln_solid"></div>	
                       
                                              <a id="popover" data-toggle="tooltip" data-placement="top"  title="to add a tag please click the button to generate a tag for the speficifed category, this action will make sure the new tags are available for selection when creating a discoverable">  <button type="submit" name="add" class="btn  btn-success">ADD TAGS</button></a>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              
                                              
                    
                    </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->             
              
                   <div class="tab-pane" id="create_ambassador">
               
                <div class="box-body">
                  <form id="createuser" method="POST" action="manage-users">
             <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tags / Interests</h3>
                   </div><!-- /.box-header -->
                    <?
					 $counter=0; 
                     if(count($query_list_tags1)>0)  {		
                      foreach ($query_list_tags1 as $item1 ) {
					  if (($counter + 1) % 6 == 0) {  echo '<br style="clear:both;"><br>'; }			   
					  $objectRef=$item1->getObjectId();  
					  $img=$item1->imageName;  
					  
					
					  
					 	 
					 if (file_exists('../interests-images/'.$img.'.jpg')) { $imagepath='../interests-images/'.$img.'.jpg'; }  
					 
					 else if(file_exists('../interests-images/'.$img.'.png')) { $imagepath='../interests-images/'.$img.'.png';} 
					 
					 else  { $imagepath='dist/img/no_pic2.png'; }
					    
				
				
					    
					 ?>
                       
                       
                     <div class="col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon">
                      <i class="fa fa-tags"></i>
                        </span>
                       <a id="popover" data-toggle="tooltip"   title="<?=$item1->get('title')?>">  
                        <input type="text" class="form-control"  value="<?=$item1->get('title')?>"></a>
                       
                      </div><!-- /input-group -->
                     
                      <br>
                      <a style="color:#000" class="image-popup-vertical-fit" href="<?=$imagepath?>" title="<?=$img?>"> 
                      <span id="popover" data-toggle="tooltip"  title="<?=$img ?>"><i class="fa fa-eye"></i></span>
                      </a>
                      
              &nbsp;&nbsp;
              <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;color:#000;" title="upload new image / change tag name"> 
              <span data-id="<?=$objectRef?>" data-ref="<?=$img?>" data-href="manage-tags?ID=<?=$objectRef?>&imagestring=<?=$img?>&flag=edit" data-toggle="modal" data-target="#confirm-delete2"><i class="fa fa-upload"></i></span></a>
              &nbsp;&nbsp;
                       
          <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;color:#000;" title="delete Interest tag">      
           <span data-id="<?=$objectRef?>" data-href="manage-tags?ID=<?=$objectRef?>&flag=deactivate" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash"></i></span>
                         
</a>
                         
                    </div><!-- /.col-lg-6 -->   
                                    
                    <? $counter++;  } } ?>
                     <br style="clear:both"><br>
                     <br><br>
                    
                     <div class="ln_solid"></div>
                    <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Browsing Tags</h3>
                   </div><!-- /.box-header -->
                 
                   <?
				     $counter=0;
                     if(count($query_list_tags2)>0)  {		
                     foreach ($query_list_tags2 as $item2 ) { 
				     if (($counter + 1) % 6 == 0) {  echo '<br style="clear:both;"><br>';  }		
					 $objectRef2= $item2->getObjectId();  
					 $img2=$item2->imageName;  
					 
					 if (file_exists('../browsing-images/'.$img2.'.jpg')) { $imagepath2='../browsing-images/'.$img2.'.jpg'; }  
					 
					 else if(file_exists('../browsing-images/'.$img2.'.png')) { $imagepath2='../browsing-images/'.$img2.'.png';} 
					 
					 else  { $imagepath2='dist/img/no_pic2.png'; }
					    
					 
					 ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon">
                      <i class="fa fa-tags"></i>
                        </span>
                       <a id="popover" data-toggle="tooltip"  title="<?=$item2->get('title')?>">  
                        <input type="text" class="form-control"  value="<?=$item2->get('title')?>"></a>
                       
                      </div><!-- /input-group -->
                       <br>
                         <a style="color:#000" class="image-popup-vertical-fit" href="<?=$imagepath2?>" title="<?=$img?>"> 
                         <span id="popover" data-toggle="tooltip"  title="<?=$img2?>"><i class="fa fa-eye"></i></span>
                         </a>
                         
                         &nbsp;&nbsp;
                         
                              <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;color:#000;" title="upload new image / change tag name"> 
            
                         <span data-id="<?=$objectRef2?>" data-ref="<?=$img?>" data-href="manage-tags?ID=<?=$objectRef2?>&imagestring=<?=$img?>&flag=edit" data-toggle="modal" data-target="#confirm-delete2b"><i class="fa fa-upload"></i></span></a>&nbsp;&nbsp;
                         
                         
                         
                         
                      <a id="popover" data-toggle="tooltip" data-placement="top"  style="cursor:pointer;color:#000;" title="delete Browsing tag">         
                       <span data-id="<?=$objectRef2?>" data-href="manage-tags?ID=<?=$objectRef2?>&flag=deactivate" data-toggle="modal" data-target="#confirm-deleteb"><i class="fa fa-trash"></i></span></a>
                    </div><!-- /.col-lg-6 --> 
                    <? $counter++; } } ?>
                    
                    
                    
                    <br><br> 
                    
               <!--      <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            
                                     
                                            
                                            <a id="popover"data-toggle="tooltip" data-placement="top"   title="to edit a tag please input a different title into the text box and ensure its selected,you are allowed multiple options ">   <button type="submit" name="edit" class="btn btn-primary">EDIT SELECTED TAGS</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              <a id="popover" data-toggle="tooltip" data-placement="top"  title="to delete a tag please  ensure its selected you are allowed multiple options">  <button type="submit" name="delete" class="btn  btn-danger">DELETE SELECTED TAGS</button></a>
                                            </div>
                                        </div>-->
                                        
                                        </form>
                          
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
         
            </div><!-- /.box-body -->
            
               
          
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
        <div class="example-modal5" style="display:none;" id="confirm-delete">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will delete the Interest tag from the system</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
            <div class="example-modal5b" style="display:none;" id="confirm-deleteb">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">This will delete the Browsing tag from the system</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
          
            <div class="example-modal6" style="display:none;" id="confirm-delete2">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Edit Interest Tag</h4>
                      <p id="createformresults">   </p>
                  </div>
                  <div class="modal-body">
                   <form id="myFormSubmit" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="tag_id" id="tag_id">
                     <input type="hidden" name="imagestring" id="imagestring">
                   
                    <p> Upload new image </p>
                    <input type="file" name="file" id="file"><br>
                   <p> Give the image / interest tag a new title </p>
                   <input type="text" name="tag_title" id="tag_title" placeholder="new tag name" style="padding:3px; width:180px;">
                 <br><br><p> Your editing TAG  </p>
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary btn-ok2" data-dismiss="modal">Cancel</button>
                  
                     <button type="submit" class="btn btn-success btn-ok" >Submit data</button>
                  </div>
                    </form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
            <div class="example-modal6b" style="display:none;" id="confirm-delete2b">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Edit browsing Tag</h4>
                        <p id="createformresults2">   </p>
                  </div>
                  <div class="modal-body">
               <form id="myFormSubmit2" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="tag_id2" id="tag_id2">
                      <input type="hidden" name="imagestring2" id="imagestring2">
                   
                    <p> Upload new image </p>
                    <input type="file" name="file" id="file"><br>
                   <p> Give the image / browsing tag a new title </p>
                   <input type="text" name="tag_title" id="tag_title" placeholder="new tag name" style="padding:3px; width:180px;">
                 <br><br><p> Your editing TAG  </p>
                     <p class="debug-url"></p>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary btn-ok2" data-dismiss="modal">Cancel</button>
                  
                     <button type="submit" class="btn btn-success btn-ok" >Submit data</button>
                  </div>
                    </form>
                    </form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
      

           <? 
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
   <script>
 // Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})
</script>

     <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  ?>

<script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Image ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>
    <script>
	$('#confirm-deleteb').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	     $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
      $('.debug-url').html('Image ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
        });
	</script>

    
    
    <script>  
   $('#myFormSubmit').on('submit', function(e){
     e.preventDefault();
    $.ajax({
        url: 'includes/process_edit_interest_tags.php',
        data: new FormData( this ),
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
		$('#createformresults').text(data);
         console.log(data);
        }
    });
    });
 </script>
 
 
 
    
 <script>  
   $('#myFormSubmit2').on('submit', function(e){
     e.preventDefault();
    $.ajax({
        url: 'includes/process_edit_browsing_tags.php',
        data: new FormData( this ),
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
		$('#createformresults2').text(data);
         console.log(data);
        }
    });
    });
 </script>
 

 
  <script>
	$('#confirm-delete2').on('show.bs.modal', function(e) {
       
	    $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
		
		 $(this).find('.btn-ok2').attr('id', $(e.relatedTarget).data('ref'));
		  
        $('.debug-url').html('Tag ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
	    $('#tag_id').val($(this).find('.btn-ok').attr('id'));
		  $('#imagestring').val($(this).find('.btn-ok2').attr('id'));
        });
		
		
	</script>
    
       <script>
	$('#confirm-delete2b').on('show.bs.modal', function(e) {
		  
	    $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
	    $(this).find('.btn-ok2').attr('id', $(e.relatedTarget).data('ref'));
		
		
      $('.debug-url').html('Image ID: <strong>' + $(this).find('.btn-ok').attr('id') + '</strong>');
	   $('#tag_id2').val($(this).find('.btn-ok').attr('id'));
	    $('#imagestring2').val($(this).find('.btn-ok2').attr('id'));
        });
	</script>


 

    <script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>

<script>

function closeMe2(element) {
  $(element).parent().remove();
}


function closeMe1(element) {
  $(element).parent().remove();
}

function closeMe1b(element) {
  $(element).parent().remove();
}

function addTag() {
  var container = $('#list1');
  var item1 = container.find('.default').clone();
  item1.removeClass('default');
  
  var container2 = $('#list1b');
  var item2 = container2.find('.default2').clone();
  item2.removeClass('default2');
  //add anything you like to item, ex: item.addClass('abc')....
  item1.appendTo(container).show();
  item2.appendTo(container2).show();
}


function addTag2() {
  var container = $('#list2');
  var item1 = container.find('.default').clone();
  item1.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item1.appendTo(container).show();
  
  var container2 = $('#list2b');
  var item2 = container2.find('.default2b').clone();
  item2.removeClass('default2b');
  //add anything you like to item, ex: item.addClass('abc')....
  item1.appendTo(container).show();
  item2.appendTo(container2).show();
  
}



</script>  

<script>
$(document).ready(function() {
$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}
		
	});
});	
</script>

 <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>

     
  </body>
</html>