<? 
//upload class

include_once('class.upload.php');
//include the use of the classes in this script
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseClient;


$query_contributor = new ParseQuery("Interest");
$query_contributor->descending("createdAt");
$query_contributor->limit(1);
$results_contributor = $query_contributor->find();
if(count($results_contributor)>0)  {		
foreach ($results_contributor as $contributor ) {  
	 $imageName=$contributor->get('imageName');
	 $jar_id=$contributor->get('jarId'); 
 }
}


if (isset($_POST['itags']) && sizeof($_POST['itags']>0)) {
	
	unset($_POST['itags'][0]);	
	$interestarray=$_POST['itags'];
	
	foreach($interestarray as $tag)  {		
	$array_interest_tags[]=$tag;
    $array_tags_desc=$array_interest_tags;
		
	}
}

if (isset($_FILES['interest_img']) && sizeof($_FILES['interest_img']>0)) {
	
	unset($_FILES["interest_img"]["tmp_name"][0]);   
	$pathStore1='../interests-images/';
	
	foreach($_FILES['interest_img']['tmp_name'] as $key => $tmp_name) {
   
    $file_name = $_FILES['interest_img']['name'][$key];
	$fname = pathinfo($file_name, PATHINFO_FILENAME);
	$ext = pathinfo($file_name, PATHINFO_EXTENSION);
    $file_size =$_FILES['interest_img']['size'][$key];
    $file_tmp =$_FILES['interest_img']['tmp_name'][$key];
    $file_type=$_FILES['interest_img']['type'][$key]; 
	
	$result=get_numerics($imageName);
	$jarID=$jar_id+1;
    $num1= $result[0]+1;
    $num2= $result[1];
	
    $imagestring='interest-'.$num1.'-'.$num2.'-'.$fname.'.'.$ext;
	$imagestringdb='interest-'.$num1.'-'.$num2.'-'.$fname;
	
	$array_interest_img_temp[]=$file_tmp;
    $array_img_tmp=$array_interest_img_temp;
	
	$array_interest_img_name[]=$imagestringdb;
    $array_img_name=$array_interest_img_name;
	
    move_uploaded_file($file_tmp,$pathStore1.$imagestring);
}
	
	
    $output=array_combine($array_tags_desc,$array_img_name);
   
   	foreach($output as $key=>$val)   {  	
	
	$new_tag = new ParseObject("Interest");
	$new_tag->set("imageName",$val);
	$new_tag->set("jarId",$jarID);
    $new_tag->set("title", $key);
	
	try  {
    $new_tag->save();
    $insert1=true;
	$success='The tags have been processed and created';
   		
} catch (ParseException $ex) {
  $error.='<li>The tag could not be created</li> ';
  echo $error;
  echo $ex->getCode;
  echo $ex->getMessage;
}

}
	
	
}


if (isset($_POST['btags']) && sizeof($_POST['btags']>0)) {
	
	unset($_POST['btags'][0]);	
	$browsearray=$_POST['btags'];
	
	foreach($browsearray as $tag)  {		
	$array_browse_tags[]=$tag;
    $array_tags_desc2=$array_browse_tags;
		
	}
}

if (isset($_FILES['browsing_img']) && sizeof($_FILES['browsing_img']>0)) {
	
	unset($_FILES["browsing_img"]["tmp_name"][0]);   
	
	$pathStore2='../browsing-images/';
	
	foreach($_FILES['browsing_img']['tmp_name'] as $key => $tmp_name) {
   
    $file_name = $_FILES['browsing_img']['name'][$key];
	$fname = pathinfo($file_name, PATHINFO_FILENAME);
	$ext = pathinfo($file_name, PATHINFO_EXTENSION);
    $file_size =$_FILES['browsing_img']['size'][$key];
    $file_tmp =$_FILES['browsing_img']['tmp_name'][$key];
    $file_type=$_FILES['browsing_img']['type'][$key]; 
	
	
		
	$array_interest_img_temp[]=$file_tmp;
    $array_img_tmp=$array_interest_img_temp;
	
	$array_browse_img_name[]=$fname;
    $array_img_name2=$array_browse_img_name;
	
    move_uploaded_file($file_tmp,$pathStore2.$file_name);
}
	
	
    $output=array_combine($array_tags_desc2,$array_img_name2);
   
   	foreach($output as $key=>$val)   {  	
	
	$new_tag = new ParseObject("browsingTags");
	$new_tag->set("imageName",$val);
    $new_tag->set("title", $key);
	
	try  {
    $new_tag->save();
    $insert1=true;
	$success='The tags have been processed and created';
   		
} catch (ParseException $ex) {
  $error.='<li>The tag could not be created</li> ';
  echo $error;
  echo $ex->getCode;
  echo $ex->getMessage;
}

}
	
	
}



