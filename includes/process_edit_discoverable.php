<? 
//upload class
include('class.upload.php');
//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseGeoPoint;
$insert1=false;
$insert2=false;
$uniqueFn='';
$array_content_tips2='';
$array_content_tips1='';


  //lets prepare the form inputs
  $keysToExtract = array('title', 'sub_title', 'discoverable_desc', 'am_image',  'dteexpiry', 'address', 'lat', 'dteTime','lng','cost', 'contact', 'type', 'timeslotstring', 'ID');
  //extract the post array
  extract($_POST);
  //loop through and get values 

  foreach ($keysToExtract as $key) {									  	      
		if(!empty($_POST[$key])) { 
			$$key =$_POST[$key];
			}
		}


$imagestring=$am_image;

//reverse geocode the address to get variables 		
$myLocation = reverse_geocode($address);
$pieces = explode("/", $myLocation);

if(isset($pieces[0]) && !empty($pieces[0])) {
$country=$pieces[0];
}
if(isset($pieces[1]) && !empty($pieces[1])) {
$region=$pieces[1];	
}
if(isset($pieces[2]) && !empty($pieces[2])) {
$city=$pieces[2];
}


$jamjarcity=$_POST['jamjarcity'];	

$object_ref_ambassador=$_POST['allocated_ambassador'];	


//build image name string and ttite formatting
if(!empty($imageName)) {
$result=get_numerics($imageName);
$num1= $result[0]+1;
$num2= $result[1];
$titlestring = str_replace(' ', '_', $title);
$imagestring='discoverable-'.$num1.'-'.$num2.'-'.$titlestring;
}

else {
	$titlestring = str_replace(' ', '_', $title);
}

//build tags array element 
if (isset($_POST['tags']) && sizeof($_POST['tags']>0)) {
$tags_array=$_POST['tags'];	
foreach($tags_array as $item_array)   {
$array_content_interests[]= array('__type' => 'Pointer','className' => 'Interest','objectId' =>''.$item_array.'');	 
}
}


if (isset($_POST['tips']) && sizeof($_POST['tips']>0)) {
$tips_array=array_filter($_POST['tips']);	

foreach($tips_array as $item_array3)   {
$array_content_tips1[]= $item_array3;	
}
}


//build tips array 
if (isset($_POST['tipsnew']) && sizeof($_POST['tipsnew']>0)) {
$tips_array=$_POST['tipsnew'];	
unset($tips_array[0]);
foreach($tips_array as $item_array3)   {
$array_content_tips2[]= $item_array3;
if($_POST['tips'] && sizeof($_POST['tips']>0)) {
$array_tips = array_merge($array_content_tips1, $array_content_tips2);
}

}
}


//error flag 
$errorflag=0; 

//format vals for parse insert 
$lat_val = (float)$lat;
$lng_val = (float)$lng;
$type_val = (int)$type;


//geocode vals
$geopoint = new ParseGeoPoint($lat_val, $lng_val);

if(!empty($type_val) && $type_val==2 || $type_val==3)  {
	$array_date =$dteTime;	
    if(sizeof($_POST['dteTimeExtra'])>0)  {
	$array_date2=array_filter($_POST['dteTimeExtra']); 
	$result_array = array_merge($array_date, $array_date2);
	
	}
	else {
	$result_array=$array_date;	
	}
}



if(sizeof($result_array>0))  {
require ('timeslots_logic.php');
}

$uniqueFn=$ID.'_'.uniqid();


if (sizeof($_POST['tags'])==0) {
	
 $errorflag=1;
 $error.='<li>At least 1 Interest tag is required when creating a discoverable </li> ';
	
}

if (sizeof($_POST['tags2'])==0) {
	
 $errorflag=1;
 $error.='<li>At least 1 Browsing tag is required when creating a discoverable </li> ';
	
}
 



//lets process teh uploaded file resize if needed and store 
  if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {	
 

  
	$tmpFile = $_FILES["file"]["tmp_name"];
    $fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];	
	//various paths to store the images 
	$pathStore1='../discoverable-images/256/';
	$pathStore2='../discoverable-images/512/';
   if($fileSize > 5242880) { // if file size is larger than 5 Megabytes
       $error.= "<li>ERROR: Your file was larger than 5 Megabytes in size.</li>";
	   $errorflag=1;
       unlink($tmpFile); 
	}	
   if (!preg_match("/.(jpeg|jpg|png)$/i", $fileName) ) {
     $error.= "<li>ERROR: Your image was not  .jpg, or .jpeg or .png.</li>";
	 $errorflag=1;
     unlink($tmpFile);    
     }  
   if($errorflag==0) {
	  if (file_exists('../discoverable-images/256/'.$imagestring.'.jpg')) { 
	  unlink('../discoverable-images/256/'.$imagestring.'.jpg'); 
	 }  
	 
	 if (!file_exists('../discoverable-images/256/'.$imagestring.'.jpg')) { 
         $handle1 = new Upload($_FILES['file']);     
     if ($handle1->uploaded) {
        $handle1->image_resize          = true;
		$handle1->image_ratio           = true;
		$handle1->image_y               = 256;		
		$handle1->image_x               = 256;
		$handle1->file_new_name_body = $uniqueFn;
        $handle1->Process($pathStore1);
      }
 }
	  
	    if (file_exists('../discoverable-images/512/'.$imagestring.'.jpg')) { 
	     unlink('../discoverable-images/512/'.$imagestring.'.jpg'); 
	 }  
	  if (!file_exists('../discoverable-images/512/'.$imagestring.'.jpg')) { 
        $handle2 = new Upload($_FILES['file']);     
	    if ($handle2->uploaded) {
        $handle2->image_resize          = true;
		$handle2->image_ratio           = true;
		$handle2->image_y               = 512;		
		$handle2->image_x               = 512;
		$handle2->file_new_name_body = $uniqueFn;
        $handle2->Process($pathStore2);
      }
    }
  } 
}	

else {
	$imagestring='';
}

$new_city = new ParseQuery("Cities");
$new_city->equalTo("cityName", $jamjarcity);
$new_city->equalTo("country", $country);
$new_city->limit(1);
$results_city = $new_city->find();
if(count($results_city)==0)  {	

//lets insert the new city on the fly 

$create_city = new ParseObject("Cities");
$create_city->set("cityName", $jamjarcity);
$create_city->set("state", $region);
$create_city->set("country", $country);

try  {
  $create_city->save();
  $insert1=true;
  $object_ref_city=$create_city->getObjectId();
 		
} catch (ParseException $ex) {
  $error.='<li>The city could not be created</li> ';
  //echo $ex->getCode;
  //echo $ex->getMessage;
}

} 

else  {		
     foreach ($results_city as $city ) {  
     $object_ref_city=$city->getObjectId();
	 }
 }
	
if($errorflag==0) {	

$new_discover = new ParseQuery("Discoverable");
$new_discover->equalTo("objectId", $ID);
$row = $new_discover->first();
$row->set("address", $address);
$row->set("canonicalTitle", $title);
$row->set("contact", $contact);
$row->set("detail", $discoverable_desc);
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {	 
$row->set("imageName", $uniqueFn);
}
$row->set("inactive", false);
$row->setAssociativeArray("contributor", array('__type' => 'Pointer', 'className' => 'Contributor', 'objectId' =>$object_ref_ambassador));
if (isset($_POST['tags']) && sizeof($_POST['tags']>0)) {
$row->setArray('interests', $array_content_interests);
}
$row->set("locationLatitude", $lat_val);
$row->set("locationLongitude", $lng_val);
$row->set("locationName", '');
$row->set("price", $cost);
$row->set("subtitle", $sub_title);
if (isset($Timeslotsarray) && sizeof($Timeslotsarray>0)) {
$row->set("timeslotsString", $timeslotstring);
$row->setArray("timeslotsArray", $Timeslotsarray);
$row->set("expirationTimesslot", $expiryDte );
}
$row->set("jamjarCity", $jamjarcity);
$row->set("title", $title);
$row->set("type", $type_val);
$row->setAssociativeArray("cityRef", array('__type' => 'Pointer', 'className' => 'Cities', 'objectId' =>$object_ref_city));

if (isset($array_tips) && sizeof($array_tips>0) ) {
$row->setArray('tips', $array_tips);
}
$row->set("loc", $geopoint);
try  {
  $row->save();
    $insert1=true;
    $object_ref=$row->getObjectId();	
	
	
	//new object 
	$discoverable = new ParseObject("Discoverable", $object_ref);
	
/*	
	if (isset($_POST['tags2']) && sizeof($_POST['tags2']>0)) {
    $tags_array2=$_POST['tags2']; 
	foreach($tags_array2 as $tagitem2)   {  	
	if (!in_array($tagitem2,$btag_object))  {	
	$tag2 = new ParseObject("browsingTags", $tagitem);
	$relation2 = $tag2->getRelation("discoverables");
	$relation2->remove($discoverable);
	$tag2->save();
	}
	}
}
*/	

	//check for tags relation ad add relations
    if (isset($_POST['tags2']) && sizeof($_POST['tags2']>0)) {
    $tags_array2=$_POST['tags2']; 
	foreach($tags_array2 as $tagitem)   {  	
	$tag = new ParseObject("browsingTags", $tagitem);
	$relation = $tag->getRelation("discoverables");
    $relation->add($discoverable);
	$tag->save();
	}
	}
} catch (ParseException $ex) {
  $error.='<li>the discoverable could not be edited</li> ';
  echo $ex->getCode;
  echo $ex->getMessage;
}

}

if($insert1==true)  {	
  $success.='<li>The discoverable has been edited successfully</li>'; 
  
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
	 $success.='<li>The Image was updated successfully</li>';
  }
  
 }
 
 
 


