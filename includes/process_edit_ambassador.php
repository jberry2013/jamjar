<? 

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;

$insert1=false;
$insert2=false;
$uniqueFn='';



  //lets prepare the form inputs
  $keysToExtract = array('am_name', 'am_email', 'am_location', 'am_url' , 'am_bio', 'ID', 'am_image');
  //extract the post array
  extract($_POST);
  //loop through and get values 

  foreach ($keysToExtract as $key) {									  	      
		if(!empty($_POST[$key])) { 
			$$key =$_POST[$key];
			}
		}
		
		
$imagestring=$am_image;
		
if(!isset($_POST['access'])) { $admin_level=false; } else { $admin_level=true; }

if(trim($_POST['password'])==$_SESSION['login_password']) {
	$pass=$_SESSION['login_password'];
}
else {
	$pass=$_POST['password'];
}

//lets process teh uploaded file resize if needed and store 
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
	
	
	 
	$tmpFile = $_FILES["file"]["tmp_name"];
    $fileName = $_FILES["file"]["name"];
	$fnamebody=pathinfo($fileName, PATHINFO_FILENAME);
	$fnameext=pathinfo($fileName,PATHINFO_EXTENSION);
	
	//echo $fnameext;
	$fileSize = $_FILES["file"]["size"];	
	//various paths to store the images 
	$pathStore1='../ambassador-images/256/';
	$pathStore2='../ambassador-images/Lge/';
	$uniqueFn=$ID.'_'.uniqid();
	
	//usage example
     
	
	if($fileSize > 5242880) { // if file size is larger than 5 Megabytes
       $error.= "<li>ERROR: Your file was larger than 5 Megabytes in size.</li>";
	   $errorflag=1;
       unlink($tmpFile); 
	}	
	if (!preg_match("/.(jpeg|jpg|png)$/i", $fileName) ) {
     $error.= "<li>ERROR: Your image was not  .jpg, or .jpeg or .png.</li>";
	 $errorflag=1;
     unlink($tmpFile);    
     }  
  
  
   if($errorflag==0) {
	  if (file_exists('../ambassador-images/256/'.$imagestring.'.'.$fnameext)) { 
	  unlink('../ambassador-images/256/'.$imagestring.'.jpg'); 
	 }  
	 	 
	  if (!file_exists('../ambassador-images/256/'.$imagestring.'.'.$fnameext)) { 
     
    	//move_uploaded_file($_FILES['file']['tmp_name'], $pathStore1.$fileName);
		
		
		$im = new ImageManipulator($_FILES['file']['tmp_name']);
		
		
			$width=round($im->getWidth());
			$height=round($im->getHeight());
			
			$mediumHeight = 0;
            $mediumWidth = 600;
			
			
		  	 
           if($width > 600) {
             // setup height to be proportional to the original so we don't get stretching.
             $mediumHeight = ($mediumWidth * $height) / $width;
 
             // resize the image in the manipulator class.
             $im->resample($mediumWidth, $mediumHeight);
		 
		     $im->save($pathStore2.$_FILES["file"]["name"]);
         
		    }

			
		    $offset = 0;
 
		// determine which is the longer side.
		if($width > $height) {
			// setup an offset for the width as it is larger
			$offset = ($width - $height) / 2;
			$newImage = $im->crop($offset, 0, $height + $offset, $height);
							 
		} else {
			// setup an offset for the height as it is larger
			$offset = ($height - $width) / 2;
			$newImage = $im->crop(0, $offset, $width, $width + $offset);
		}
 
             // resize the cropped, square image to a thumbnail size.
           $im->resample(200, 200);
 
		
	
			$im->save($pathStore1.$_FILES["file"]["name"]);

      
		
	//	$imgsource="http://www.jamjarapp.com.au/ambassador-images/256/" . $_FILES["file"]["name"];
		
	
	

			
	
	
        } 
 
    } 
 }	

//check url

if(!empty($am_url)) {
$test_url=validateURL($am_url);
if($test_url==true)  {
$am_url2=$am_url;
}

else {
	 $error.= "<li>ERROR: The URL provided is not valid please try again</li>";
	 $errorflag=1;
}

}

if($errorflag==0) {	
$new_contributor = new ParseQuery("Contributor");
$new_contributor->equalTo("objectId", $ID);
$row = $new_contributor->first();
$row->set("URL", $am_url2);
$row->set("detail", $am_bio);
$row->set("name", $am_name);
$row->set("location", $am_location);
$row->set("emailAddress", $am_email);
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
$row->set("imageName", $fnamebody);
}
try  {
  $row->save();
 // echo 'Object correctly saved and created - ' .$new_contributor->getObjectId();
  $insert1=true;
 		
} catch (ParseException $ex) {
  $error.='<li>the ambassador could not be edited</li> ';
  echo $ex->getCode;
  echo $ex->getMessage;
}

if($insert1==true) {	
$query_contributor2 = new ParseQuery("CrmUsers");
$query_contributor2->includeKey("relObjectID");
$query_contributor2->equalTo("relObjectID", array("__type" => "Pointer", "className" => "Contributor", "objectId" => $ID));
$row2 = $query_contributor2->first();
$row2->set("adminAccess", $admin_level);
$row2->set("password", $pass);
$row2->set("username", $am_email);


try  {
  $row2->save(); 
  $insert2=true;
		
} catch (ParseException $ex2) {
    // $error.='The Ambassador access could not be created could not be created ';
     echo $ex2->getCode;
     echo $ex2->getMessage;
  }	
}

}

if($insert2==true)  {	
  $success.='<li>The Ambassador has been edited successfully</li>'; 
  
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
	 $success.='<li>The Image was updated successfully</li>';
  }
  
 }
 
 
 


