 <? if (file_exists('../ambassador-images/256/'.$_SESSION['userImage'].'.jpg')) { $imageuser='../ambassador-images/256/'.$_SESSION['userImage'].'.jpg';	    }  
else  { $imageuser='dist/img/no_pic2.png'; }	?>
     <!-- Logo -->
        <a href="dashboard" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="dist/img/jamjaricon2.png"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Jamjar</b> CRM </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
           <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=$imageuser?>" class="img-circle" alt="User Image"  style="width:20px; height:19px;" />
                  <span class="hidden-xs"> <?=$_SESSION['name'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=$imageuser?>" class="img-circle" alt="User Image"  style="width:84px; height:84px;" />
                    <p>
                      <?=$_SESSION['name'];?>
                       <? if($_SESSION['admin_level']==false)   echo '<small> Ambassador</small>'; ?>
                           <? if($_SESSION['admin_level']==true)   echo '<small> Administrator</small>'; ?>
                      <small>Member since <?=$_SESSION['datecreated'];?></small>
                    </p>
                  </li>            
                  <!-- Menu Footer-->
                  <li class="user-footer">
                     <div class="pull-right">
                      <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>