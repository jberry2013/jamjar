<? 
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
$errorflag=0;
//PROCESS POST REQUEST	
$username=trim($_POST['username']);
$password=trim($_POST['password']);
//INIT QUERY OBJECT	
$query = new ParseQuery("CrmUsers");
$query->includeKey("relObjectID");
$query->equalTo("username", $username);
$query->equalTo("password", $password);

try {
	$results = $query->find();
	
		
	//print_r($results);
	if(count($results)>0)  {		
	   foreach ( $results as $element ) {	
		//ok we have teh contributor data set 
		
		$_SESSION['object_ref']= $element->get("relObjectID")->getObjectId();	
		$_SESSION['userImage']=$element->get("relObjectID")->get('imageName'); 		
		$_SESSION['name']=$element->get("relObjectID")->get('name'); 
		$_SESSION['bio']=$element->get("relObjectID")->get('detail'); 
		$_SESSION['url']=$element->get("relObjectID")->get('URL'); 
		$_SESSION['admin_level']=$element->get("adminAccess"); 
		$_SESSION['inactive']=$element->get("inactive"); 
		$_SESSION['location']=$element->get("relObjectID")->get('location'); 
		$dte=$element->get("relObjectID")->getCreatedAt(); 
		$_SESSION['datecreated'] =date_format($dte, 'd-m-Y');	
		$_SESSION['login_username']=$username; 
		$_SESSION['login_password']=$password; 
		
		
	}
	
	if($_SESSION['inactive']==true) {
		 $error.= "<li> You dont currently have access to the JamJar CRM </li>";
         $errorflag=1;	
		
		}
		else {
	    header('location: '.$base.'/dashboard');
		}
 } else  {
	  $error.= "<li> ERROR: Your details are incorrect, please check and try again</li>";
      $errorflag=1;
 }
 
		} catch (ParseException $error) {
		  // $error is an instance of ParseException with details about the error.
		  echo $error->getCode();
		  echo "<br />";
		  echo $error->getMessage();
		}
		
		
?>