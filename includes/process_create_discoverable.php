<? 
//upload class
include_once('class.upload.php');
//include the use of teh classes in this script
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseClient;
use Parse\ParseGeoPoint;


	
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
	

$result_array='';
//lets get the last jar id
$query_discover = new ParseQuery("Discoverable");
$query_discover->descending("createdAt");
$query_discover->limit(1);
$results_discover = $query_discover->find();
if(count($results_discover)>0)  {		
foreach ($results_discover as $item ) {  
     $jar_id=$item->get('jarId'); 
	 $imageName=$item->get('imageName');
	 $lastinsertid=$item->getObjectId();
 }
}

if(empty($imageName)) {
$imageName='';	
}
//format jar id add 1 
$jarID=$jar_id+1;
$insert1=false;
$insert2=false;
  //lets prepare the form inputs
  $keysToExtract = array('title', 'sub_title', 'discoverable_desc', 'dteexpiry', 'address', 'lat', 'dteTime','lng','cost', 'contact', 'type', 'timeslotstring');
  //extract the post array
  extract($_POST);
  //loop through and get values 
  
   //print_r($_POST['dteTimeExtra']);

  foreach ($keysToExtract as $key) {									  	      
		if(!empty($_POST[$key])) { 
			$$key =$_POST[$key];
			}
		}
//reverse geocode the address to get variables 		
$myLocation = reverse_geocode($address);
$pieces = explode("/", $myLocation);

if(isset($pieces[0]) && !empty($pieces[0])) {
$country=$pieces[0];
}
if(isset($pieces[1]) && !empty($pieces[1])) {
$region=$pieces[1];	
}
if(isset($pieces[2]) && !empty($pieces[2])) {
$city=$pieces[2];
}


$jamjarcity=$_POST['jamjarcity'];	

//build image name string and ttite formatting
if(!empty($imageName)) {
$result=get_numerics($imageName);
$num1= $result[0]+1;
$num2= $result[1];
$titlestring = str_replace(' ', '_', $title);
$imagestring='discoverable-'.$num1.'-'.$num2.'-'.$titlestring;
}

else {
	$titlestring = str_replace(' ', '_', $title);
}


//build tags array element 
if (isset($_POST['tags']) && sizeof($_POST['tags']>0)) {
$tags_array=$_POST['tags'];	
foreach($tags_array as $item_array)   {
$array_content_interests[]= array('__type' => 'Pointer','className' => 'Interest','objectId' =>''.$item_array.'');	 
}
}


//build tips array 
if (isset($_POST['tips']) && sizeof($_POST['tips']>0)) {
$tips_array=array_filter($_POST['tips']);	

foreach($tips_array as $item_array3)   {
$array_content_tips[]= $item_array3;	
}
}


//print_r($array_content_tips);

//error flag 
$errorflag=0; 

//format vals for parse insert 
$lat_val = (float)$lat;
$lng_val = (float)$lng;
$type_val = (int)$type;


//geocode vals
$geopoint = new ParseGeoPoint($lat_val, $lng_val);

if(!empty($type_val) && $type_val==2 || $type_val==3)  {
	$array_date = array($dteTime);	
    if(sizeof($_POST['dteTimeExtra'])>0)  {
	$array_date2=array_filter($_POST['dteTimeExtra']); 
	$result_array = array_merge($array_date, $array_date2);
	
	}
	else {
	$result_array=$array_date;	
	}
}

if(sizeof($result_array>0))  {
require ('timeslots_logic.php');
}
//lets process teh uploaded file resize if needed and store 
  if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {	 
	$tmpFile = $_FILES["file"]["tmp_name"];
    $fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];	
	//various paths to store the images 
	$pathStore1='../discoverable-images/256/';
	$pathStore2='../discoverable-images/512/';
   if($fileSize > 5242880) { // if file size is larger than 5 Megabytes
       $error.= "<li>ERROR: Your file was larger than 5 Megabytes in size.</li>";
	   $errorflag=1;
       unlink($tmpFile); 
	}	
   if (!preg_match("/.(jpeg|jpg|png)$/i", $fileName) ) {
     $error.= "<li>ERROR: Your image was not  .jpg, or .jpeg or .png.</li>";
	 $errorflag=1;
     unlink($tmpFile);    
     }  
   if($errorflag==0) {
	   
	   
	  if (file_exists('../discoverable-images/256/'.$imagestring.'.jpg')) { 
	  unlink('../discoverable-images/256/'.$imagestring.'.jpg'); 
	 }  
	 
	 if (!file_exists('../discoverable-images/256/'.$imagestring.'.jpg')) { 
         $handle1 = new Upload($_FILES['file']);     
     if ($handle1->uploaded) {
        $handle1->image_resize          = false;
		$handle1->file_new_name_body = $imagestring;
        $handle1->Process($pathStore1);
      }
 }
   
	    if (file_exists('../discoverable-images/512/'.$imagestring.'.jpg')) { 
	     unlink('../discoverable-images/512/'.$imagestring.'.jpg'); 
	 }  
	  if (!file_exists('../discoverable-images/512/'.$imagestring.'.jpg')) { 
        $handle2 = new Upload($_FILES['file']);     
	    if ($handle2->uploaded) {
        $handle2->image_resize          = false;
		$handle2->file_new_name_body = $imagestring;
        $handle2->Process($pathStore2);
      }
    }
  } 
}	

else {
	$imagestring='';
}

$new_city = new ParseQuery("Cities");
$new_city->equalTo("cityName", $jamjarcity);
$new_city->equalTo("country", $country);
$new_city->limit(1);
$results_city = $new_city->find();
if(count($results_city)==0)  {	

//lets insert the new city on the fly 

$create_city = new ParseObject("Cities");
$create_city->set("cityName", $jamjarcity);
$create_city->set("state", $region);
$create_city->set("country", $country);

try  {
  $create_city->save();
  $insert1=true;
  $object_ref_city=$create_city->getObjectId();
 		
} catch (ParseException $ex) {
  $error.='<li>The city could not be created</li> ';
  //echo $ex->getCode;
  //echo $ex->getMessage;
}

} 

else  {		
     foreach ($results_city as $city ) {  
     $object_ref_city=$city->getObjectId();
	 }
 }
 
 
 
if (sizeof($_POST['tags'])==0) {
	
 $errorflag=1;
 $error.='<li>At least 1 Interest tag is required when creating a discoverable </li> ';
	
}

if (sizeof($_POST['tags2'])==0) {
	
 $errorflag=1;
 $error.='<li>At least 1 Browsing tag is required when creating a discoverable </li> ';
	
}
 
	
if($errorflag==0) {	

$new_discover = new ParseObject("Discoverable");
$new_discover->set("address", $address);
$new_discover->set("canonicalTitle", $title);
$new_discover->set("categorisesEvents", false);
$new_discover->set("contact", $contact);
$new_discover->setAssociativeArray("contributor", array('__type' => 'Pointer', 'className' => 'Contributor', 'objectId' =>$_SESSION['object_ref']));
$new_discover->set("detail", $discoverable_desc);
$new_discover->set("imageName", $imagestring);
 if($_SESSION['admin_level']==true) {   
 $new_discover->set("inactive", false);
 $new_discover->set("publishStatus", "Live");
 } 
 if($_SESSION['admin_level']==false) {  
 $new_discover->set("inactive", true);
 $new_discover->set("publishStatus", "Pending");
 } 
if (isset($_POST['tags']) && sizeof($_POST['tags']>0)) {
$new_discover->setArray('interests', $array_content_interests);
}
$new_discover->set("isNonDiscoverable", true);
$new_discover->set("isParent", true);
$new_discover->set("jarId", $jarID);
$new_discover->set("locationLatitude", $lat_val);
$new_discover->set("locationLongitude", $lng_val);
$new_discover->set("locationName", '');
$new_discover->set("price", $cost);
$new_discover->set("subtitle", $sub_title);
if (isset($Timeslotsarray) && sizeof($Timeslotsarray>0)) {
$new_discover->set("timeslotsString", $timeslotstring);
$new_discover->setArray("timeslotsArray", $Timeslotsarray);
$new_discover->set("expirationTimesslot", $expiryDte );

}
$new_discover->set("jamjarCity", $jamjarcity);
$new_discover->set("title", $title);
$new_discover->set("type", $type_val);
$new_discover->setAssociativeArray("cityRef", array('__type' => 'Pointer', 'className' => 'Cities', 'objectId' =>$object_ref_city));

if (isset($array_content_tips)) {
$newarraytips=$array_content_tips;	
$new_discover->setArray('tips', $newarraytips);
}
$new_discover->set("loc", $geopoint);

try  {
  //lets save the objects 	
    $new_discover->save();
    $insert1=true;
    $object_ref=$new_discover->getObjectId();
	//new object 
	$discoverable = new ParseObject("Discoverable", $object_ref);
	//check for tags relation ad add relations
    if (isset($_POST['tags2']) && sizeof($_POST['tags2']>0)) {
    $tags_array2=$_POST['tags2']; 
	foreach($tags_array2 as $tagitem)   {  	
	$tag = new ParseObject("browsingTags", $tagitem);
	$relation = $tag->getRelation("discoverables");
    $relation->add($discoverable);
	$tag->save();
	} 
}
  
 		
} catch (ParseException $ex) {
  $error.='<li>The discoverable could not be created</li> ';
  echo $ex->getCode;
  echo $ex->getMessage;
}

if($insert1==true)  {	
  $success='The Discoverable has been created Successfully';
 header('location: http://www.jamjarapp.com.au/admin-crm/manage-discoverables#all_discoverables'); 
 }
 
 
  
}