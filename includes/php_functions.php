<? 


function validateURL($URL) {
      $pattern_1 = "/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se|com.au|com.ph|ph)$)(:(\d+))?\/?/i";
      $pattern_2 = "/^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se|com.au|com.ph|ph)$)(:(\d+))?\/?/i";       
      if(preg_match($pattern_1, $URL) || preg_match($pattern_2, $URL)){
        return true;
      } else{
        return false;
      }
    }
	
function rand_passwd( $length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) {
    return substr( str_shuffle( $chars ), 0, $length );
}


 function get_numerics ($str) {
        preg_match_all('/\d+/', $str, $matches);
        return $matches[0];
    }
	
     function get_numerics_ints ($str) {
        preg_match_all('!\d+!', $str, $matches);
        return $matches[0];
    }
	
	
	
function isLoggedIn()
{
   
    if(isset($_SESSION['login_username']) && isset($_SESSION['login_password']))
    {
        return true; // the user is loged in
    } else
    {
        return false; // not logged in
    }
 
    return false;
 
}	


	function reverse_geocode($address) {
	$address = str_replace(" ", "+", "$address");
	$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
	$result = file_get_contents("$url");
	$json = json_decode($result);
	foreach ($json->results as $result) {
		foreach($result->address_components as $addressPart) {
		  if((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))
		  $city = $addressPart->long_name;
	    	else if((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
		  $state = $addressPart->long_name;
	    	else if((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
		  $country = $addressPart->long_name;
		}
	}
	
	if(($city != '') && ($state != '') && ($country != ''))
		$address = $city.', '.$state.', '.$country;
	else if(($city != '') && ($state != ''))
		$address = $city.', '.$state;
	else if(($state != '') && ($country != ''))
		$address = $state.', '.$country;
	else if($country != '')
		$address = $country;
		
	// return $address;
	return "$country/$state/$city";
}
	
	
	
function sendActivationEmail($name, $password, $email)
{

	$domain=$_SERVER['SERVER_NAME'];	
	include('class.phpmailer.php');
	
$template='<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 10px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="700"  style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#ffffff" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<img src="http://jamjarapp.com.au/admin-crm/dist/img/email_logo.png" alt=" EE logo " width="60" height="100" style="display: block;" />
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 20px 20px 40px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 18px;">
										<b>'.$name.' below are your login details </b> 
									</td>
								</tr>
								
								
								
								<tr>
									<td style="padding: 40px 20 30px 40; color: #153643; font-family: Arial, sans-serif; font-size: 12px; line-height: 17px;">
							<p>Your account information:</p>
							
							<li style="background: url(http://jamjarapp.com.au/admin-crm/dist/img/preview_tick.png) no-repeat left;
	list-style: none;
	padding-left: 15px;
	margin-left: -17px;
	line-height: 1.7em;
	color: #000000; margin-left:0px;font-size: 11px;">Your  username:<strong><span style="color:#000000"> '.$email.'</span></strong></li>
			


					<li style="background: url(http://jamjarapp.com.au/admin-crm/dist/img/preview_tick.png) no-repeat left;
	list-style: none;
	padding-left: 15px;
	margin-left: -17px;
	line-height: 1.7em;color: #000000; margin-left:0px;font-size: 11px;">Your password : <strong>'.$password.'	</strong></li>
				

<p><br><a href="http://jamjarapp.com.au/admin-crm/" style="color:#000000;"> Login here </p>


					
                       
									</td>
								</tr>
					
							</table>
						</td>
					</tr>
                    
                       <tr>
                          <td bgcolor="#357CF8" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 11px;" >
										<p><a href="#" style="color:#ffffff; text-decoration:none;">Copyright &copy; 2015   <strong>JamJarApp Ltd</strong></a> 
										&nbsp;&nbsp; <a href="#" style="color:#ffffff; text-decoration:none;">Email:   <strong>support-crm@jamjarapp.com.au</strong></a></p>
									</td>
									<td align="right" width="25%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												
												
											</tr>
									</table>
							</td>    
						</tr>
				</table>
			</td>
		</tr>
     </table>';   
      
        $mail = new PHPMailer();	
        $mail->From = 'support-crm@jamjarapp.com.au'; 
        $mail->FromName   = 'support-crm';
        $mail->AddAddress($email); // to address	
		$mail->IsHTML(true);
        $mail->AddBCC('j.r.bezza@gmail.com','BCC message details of recent new user');  
        $mail->Subject = 'Your Login Information'; 
	    $mail->Body=$template; 
		            
       if (!$mail->Send() ) {		   
       return false; 
	   
      } 
	  else {
	   return true;  
	  }

}
	
?>

