<? 
//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
//lets get the get vals 
$object_ref=trim($_REQUEST['ID']);
if(empty($object_ref))  {
header('location: '.$base.'/manage-ambassadors');	
}
//lets get the last contributor image string and the last jar id
if(!empty($object_ref))  {
$query_contributor2 = new ParseQuery("CrmUsers");
$query_contributor2->includeKey("relObjectID");
$query_contributor2->equalTo("relObjectID", array("__type" => "Pointer", "className" => "Contributor", "objectId" => $object_ref));



//count discoverables against a ambassador
$querycount = new ParseQuery("Discoverable");
$querycount->equalTo("contributor", array("__type" => "Pointer", "className" => "Contributor", "objectId" => $object_ref));
$cnt_discoverables=$querycount->count();

try {
	
	    $listUser = $query_contributor2->find();		   
	    if(count($listUser)>0)  {		 
	    foreach ($listUser as $element2 ) {		
		$am_image=$element2->get("relObjectID")->get('imageName'); 		
		$am_name=$element2->get("relObjectID")->get('name'); 
		$am_bio=$element2->get("relObjectID")->get('detail'); 
		$am_url=$element2->get("relObjectID")->get('URL'); 
        $am_created=$element2->get("relObjectID")->get('createdAt');
		$am_location=$element2->get("relObjectID")->get('location');
	    $am_email=$element2->get("relObjectID")->get('emailAddress');  
		$am_username=$element2->get('username');
		$admin_level=$element2->get('adminAccess');
		$inactive=$element2->get('inactive');
        $am_password=$element2->get('password'); 
		
		 if (file_exists('../ambassador-images/256/'.$am_image.'.jpg')) { $imagepath2='../ambassador-images/256/'.$am_image.'.jpg';	    }  
		else  { $imagepath2='dist/img/no_pic2.png'; }		
	

     }    
 } else  {
	   $error.= "<li> ERROR: Your details are incorrect, please check and try again</li>";
       $errorflag=1;
    }
 
     } catch (ParseException $error) {

		  echo $error->getCode();
		  echo "<br />";
		  echo $error->getMessage();
	}
		
}
?>
