<?php
//page id variable 
$pageid='manage-discoverables';
//core vars and logic
include'includes/application_top.php';	


if(isset($_REQUEST['admin']) &&$_REQUEST['admin']=='n' )  {
include ('includes/perms.php');	
}

//if form is submitted load processing script
if(isset($_POST['create']))  {
include_once ('includes/process_create_discoverable.php');	
}

//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;

//list of intesrests
$query_new = new ParseQuery("Interest");
$query_list_tags1 = $query_new->find();


//list of browsing tags
$query_new2 = new ParseQuery("browsingTags");
$query_list_tags2 = $query_new2->find();

//html header file 
include ('includes/header.php');

?>

<style>
.map_canvas3 {
height: 100%;
width:100%;	
}

.dataTables_wrapper .ui-toolbar{
    width: 50%;
}
</style>



<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Discoverables
          </h1>
          <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Manage Discoverables</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">

            <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                   <li class="active"><a href="#all_discoverables" data-toggle="tab">All Discoverables</a></li>
                  <li><a href="#create_discoverable" data-toggle="tab">Create discoverable</a></li>
                </ul>
           <div class="tab-content">      
            <div class="active tab-pane" id="all_discoverables">     
              <div class="box" style="border:none;">
                <div class="box-body">
                  <table id="example1" class="table">
                    <thead>
                      <tr>
                       <th style="width:10%;">Imagery</th>   
                        <th style="width:55%;">Bio</th>
                        <th style="width:10%;">Ambassador</th>
                        <th style="width:5%;">Type</th>
                         <th style="width:5%;">Status</th>
                        <th style="width:10%;">Actions</th>
                      </tr>
                    </thead>
                    <?php  include ('includes/discoverables_list.php');	?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                   
                  </div>
                   <div class="tab-pane" id="create_discoverable">
                    <div class="box" style="border:none;">
                   <div class="box-header">
                    <h3 class="box-title">Add a title and Sub title and Type</h3>
                  </div><!-- /.box-header -->
                <div class="box-body">
                     <form enctype="multipart/form-data" method="post" id="creatediscoverable" action="manage-discoverables#create_discoverable">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Discoverable Title</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Enter an approriate title   (No more than 100 characters)..."  data-msg-required="A title is required." data-rule-required="true" >
                          <p class="help-block" id="chars2">100</p>
                    </div>
                    <div class="form-group">
                      <label>Sub Title</label>
                      <input type="text" class="form-control" id="sub_title" name="sub_title"  placeholder="Enter a suitable sub-title... (No more than 100 characters)">
                       <p class="help-block" id="chars3">100</p>
                    </div>
                       <div class="form-group">
                       <label>Select an approriate type</label>
                     <select class="form-control" name="type"  id="type" data-msg-required="A type of discoverable is required." data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <option value="1">Place</option>
                        <option value="2">Event</option>
                        <option value="3">Special</option>                      
                      </select>
                   </div>
                  
                  
                  
                  <div id="choose_dates" style="display:none;">
                      
                   <div class="box-header" style="padding-left:0; padding-top:20px;">
                   <h3 class="box-title">Give the Timeslot a label and select specific dates from the start /  end date calender below </h3>
                   </div><!-- /.box-header -->
                  
                    <div class="form-group">
                    <label>Input a label for the times(s) slot </label>
                      <input type="text" name="timeslotstring" class="form-control" placeholder="Eg. Every Monday And Wednesday @ 8.30-10.30 pm" id="timestring" 
                      data-msg-required="a label for the timeslot is required." data-rule-required="true" autofocus>
                    </div><!-- /.input group -->
                 
                      
                  
                    <div class="form-group">
                    <label>start  / end </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="dteTime" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" id="date" data-msg-required="dates are required." data-rule-required="true">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  
                  <div class="form-group" id="parentsection">   
                  
                    <button type="button" class="btn btn-primary" onClick="addSlot();">add new time slot</button> 
                   </div> 
                  
                     <div class="form-group" style="display:none;" id="first">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_1" class="btn btn-primary" onClick="addSlot1();">add new time slot</button>
                  </div>
                 
                     
                      <div class="form-group" style="display:none;" id="second">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_2"  class="btn btn-primary" onClick="addSlot2();">add new time slot</button>
                  </div>
                 
                 
                         
                     <div class="form-group" style="display:none;" id="third">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_3"  class="btn btn-primary" onClick="addSlot3();">add new time slot</button>
                  </div>
                 
                      <div class="form-group" style="display:none;" id="fourth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_4"  class="btn btn-primary" onClick="addSlot4();">add new time slot</button>
                  </div>    
                  
                     <div class="form-group" style="display:none;" id="fifth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_5"  class="btn btn-primary" onClick="addSlot5();">add new time slot</button>
                  </div>  
                  
                     <div class="form-group" style="display:none;" id="sixth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_6"  class="btn btn-primary" onClick="addSlot6();">add new time slot</button>
                  </div>   
                  
                    <div class="form-group" style="display:none;" id="seven">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_7"  class="btn btn-primary" onClick="addSlot7();">add new time slot</button>
                  </div>     
                    
                    <div class="form-group" style="display:none;" id="eight">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                   
                  </div>  
                  
                  </div>   
                  
                    
         
                 
                   <div class="form-group">
                      <label for="exampleInputFile">Upload an Image / profile pic </label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                    </div>               
          
                    <!-- checkbox -->   
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title">description </h3>
                </div><!-- /.box-header -->

                    <div class="form-group">
                     
                      <textarea class="form-control" id="desc" name="discoverable_desc" rows="5" placeholder="Enter a description here  (No more than 100 words)..." data-msg-required="A description of the discoverable is required." data-rule-required="true"></textarea>
                      <p class="help-block" id="chars">500</p>
                    </div>

                 
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title">More details</h3>
                </div><!-- /.box-header -->
              
                    <!-- text input -->
                   
                    <div class="form-group">
                      <label>Address </label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="begin typing to auto complete the location" 
                      data-msg-required="An address is required." data-rule-required="true">
                    </div>
                    <div class="form-group">
                      <label>Location (lattitide and Longitude)</label>
                    </div>
                    
                    <div class="col-xs-3" style="padding-left:0;">
                      <input type="text" class="form-control" name="lat" id="lat" placeholder="latitude.." value="">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" name="lng" id="lng"  placeholder="longitude..." value="">
                    </div>
                           
                   <br style="clear:both"><br>
                    
                    <div class="map_canvas_wrap"> 
                     <div class="map_canvas3">    </div>
                    </div>
          
               
               
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Price</h3>
                   </div><!-- /.box-header -->
                    <div class="form-group">
                      <input type="text" class="form-control" name="cost" id="cost" placeholder="how much" data-msg-required="a cost is required." data-rule-required="true">
                    </div>
                    
                       
                      <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Contact Number</h3>
                   </div><!-- /.box-header -->
                    
                     <div class="form-group">
                      <input type="text" class="form-control" name="contact" id="contact" placeholder="contact number"  data-msg-required="a contact number  is required." data-rule-required="true">
                    </div>

                  <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tags / interests</h3>
                   </div><!-- /.box-header -->
                    <? 
                     if(count($query_list_tags1)>0)  {		
                      foreach ($query_list_tags1 as $item1 ) { 
					  $objectRef= $item1->getObjectId();   ?>
                     <div class="col-lg-2">
                      <div class="input-group" style="margin-left:0px;">
                        <span class="input-group-addon" >
                          <input type="checkbox" name="tags[]" value="<?=$objectRef?>">
                        </span>
                        <input type="text" class="form-control" placeholder="<?=$item1->get('title')?>">
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->                          
                    <?  } } ?>
                     <br style="clear:both"><br>
                    <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Browsing Tags</h3>
                   </div><!-- /.box-header -->
                   
                   <? 
                     if(count($query_list_tags2)>0)  {		
                      foreach ($query_list_tags2 as $item2 ) { 
					 $objectRef2= $item2->getObjectId();  ?>
                     <div class="col-lg-2">
                      <div class="input-group" style="margin-left:0px;">
                        <span class="input-group-addon">
                          <input type="checkbox"  name="tags2[]" value="<?=$objectRef2?>">
                        </span>
                        <input type="text" class="form-control" placeholder="<?=$item2->get('title')?>">
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                 
                   
                    <?  } } ?>
                       <br style="clear:both"><br>
                       
                       
                       
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tips</h3>
                   </div><!-- /.box-header -->  
                   
                   
                     <ul id="list2">
                    <li class="default" style="display: none; margin-bottom:10px; list-style-type:none;">
                        <input placeholder="new tip" type="text" name="tips[]"/><span style="cursor: pointer;" onClick="closeMe2(this);"> <i class="fa fa-close"></i></span>
                    </li>
                    </ul>
                    
                     
                            
                    <div class="form-group">   
                    <button type="button" class="btn btn-primary" onClick="addTip();">add a tip</button>
                    </div> 
                       
                    
                      <div class="form-group">
                        <div class="col-sm-1" style="float:right; margin-right:35px;">
                          <button type="submit" name="create" class="btn btn-success btn-lrg">Create discoverable</button>
                        </div>
                        </div>
                    
                  </form>
                  
                
                        <!--   <div class="col-sm-2" style="padding-left:50px;">
                          <button type="submit" name="create" class="btn btn-danger reset">Reset Form</button>
                        </div>-->
             
                  </div><!-- /.box -->       
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
       <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
        <? }  
      
     
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
   <script>
 // Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})
</script>


 <script type="text/javascript">
    $("#creatediscoverable").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>

      <script>
      $(function () {
        $("#example1").DataTable({ "iDisplayLength": 5 ,  "aLengthMenu": [5, 10, 25, 50, 100]});
			
         $('.dataTables_filter input').attr("placeholder", "Search by any of the displayed columns here to sort the list");
		   $('.dataTables_filter input').css("width", "350px");

        $('#example2').DataTable({	
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
    
    <script>
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

</script>  
<script>
var elem = $("#chars2");
$("#title").limiter(100, elem);
</script>

<script>
var elem = $("#chars3");
$("#sub_title").limiter(100, elem);
</script>
     <script>
var elem = $("#chars");
$("#bio_desc").limiter(500, elem);
</script>
    <script>
      $(function () {		  
		  var mapoptions = {
          map: ".map_canvas3",
		  details: "form",
		  types: ['geocode','establishment']
        };
        
		$('#address').geocomplete(mapoptions)		
       
});

  </script>  
  
  
    <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>
  
  
    
<script>

function closeMe2(element) {
  $(element).parent().remove();
}

function addTip() {
  var container = $('#list2');
  var item = container.find('.default').clone();
  item.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item.appendTo(container).show();
}


function addSlot() {
  $('#parentsection').hide();
  $('#first').show();
  
}

function addSlot1() {

 $('#button_1').hide();
  $('#second').show();
  
}


function addSlot2() {
 $('#button_2').hide();
  $('#third').show();
  
}

function addSlot3() {
 $('#button_3').hide();
  $('#fourth').show();
  
}

function addSlot4() {
 $('#button_4').hide();
  $('#fifth').show();
  
}

function addSlot5() {
 $('#button_5').hide();
  $('#sixth').show();
  
}

function addSlot6() {
 $('#button_6').hide();
  $('#seven').show();
  
}

function addSlot7() {
 $('#button_7').hide();
  $('#eight').show();
  
}


</script>  


 <script>
 // Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})
</script>

<script>
  $(function () {
    $('.datetimeslot').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY H:mm'});

   })
</script>  
    
   
<script type="text/javascript">
$(document).ready(function(){
    $("#type").change(function(){
		
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")==2){
                
                $("#choose_dates").show();
            }
            else if($(this).attr("value")==3){
              
                $("#choose_dates").show();
            }
           
            else{
                $("#choose_dates").hide();
            }
        });
    })
});
</script>
  
  
  </body>
</html>
