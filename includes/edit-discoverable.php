<?php
//page id variable 
$pageid='manage-discoverables';
$subpageid='edit-discoverable';
//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);


//core vars and logic
include'includes/application_top.php';	
//html header file 
include ('includes/header.php');


//if form is submitted load processing script
if(isset($_POST['edit']))  {
include ('includes/process_edit_discoverable.php');	
}

if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='deactivate' )  {
	//process the deactivate
	include('includes/deactivate.php');
}
if(isset($_GET['flag']) && isset($_GET['ID']) && $_GET['flag']=='activate' )  {
	//process the activate
   include('includes/activate.php');
}


//list of intesrests
$query_new = new ParseQuery("Interest");
$query_list_tags1 = $query_new->find();


//list of browsing tags
$query_new2 = new ParseQuery("browsingTags");
$query_list_tags2 = $query_new2->find();


//list of ambassadors
include ('includes/discoverable_data.php');	
?>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
  <header class="main-header">
      <? include ('includes/sub_header.php'); ?>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
       <!-- /.sidebar -->
       <? include ('includes/sidebar_nav.php'); ?>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Discoverable
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="manage-discoverables"><i class="fa fa-building"></i> Manage Discoverables</a></li>
            <li class="active">Edit Discoverable</li>
          </ol>
        </section>
        
     
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                 <img class="img-responsive" src="<?=$imagepath2?>" width="360px" height="360px" alt="Photo" >        
                </div><!-- /.box-body -->
              </div><!-- /.box -->

               <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Statistics</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-thumbs-up margin-r-5"></i>  Likes (145)</strong>
                 

                  <hr>
                  
                    <strong><i class="fa fa-building margin-r-5"></i>  Visits (50)</strong>
                 

                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <p class="text-muted">Perth, Australia</p>

                
                     <hr>
                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Highlights</strong>
                  <p>small bio about discoverable.....</p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
               <div class="box box-primary">
            
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Discoverable</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
            
                  <form enctype="multipart/form-data" method="post" id="editdiscoverable" action="edit-discoverable">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Discoverable Title</label>
                      <input type="text" class="form-control" id="title" name="title" value="<? if(isset($title)) echo $title ?>" placeholder="Enter an approriate title   (No more than 100 characters)..."  data-msg-required="A title is required." data-rule-required="true" >
                        <input type="hidden" class="form-control"  name="ID" id="ID"  value="<? if(isset($object_ref)) echo $object_ref ?>">
                         <input type="hidden" class="form-control" name="am_image" id="am_image"  value="<? if(isset($imageName)) echo $imageName ?>">
                          <p class="help-block" id="chars2">100</p>
                    </div>
                    <div class="form-group">
                      <label>Sub Title</label>
                      <input type="text" class="form-control" id="sub_title" name="sub_title" value="<? if(isset($subtitle)) echo $subtitle ?>"  placeholder="Enter a suitable sub-title... (No more than 100 characters)">
                       <p class="help-block" id="chars3">100</p>
                    </div>
                       <div class="form-group">
                       <label>Select an approriate type</label>
                     <select class="form-control" name="type" id="type"  data-msg-required="A type of discoverable is required." data-rule-required="true" >
                        <option value="">Choose an option</option>
                        <option value="1" <? if($type==1) echo "selected=selected"?>>Place</option>
                        <option value="2" <? if($type==2) echo "selected=selected"?>>Event</option>
                        <option value="3" <? if($type==3) echo "selected=selected"?>>Special</option>                      
                      </select>
                   </div>
                   
                   <? if($type==2 || $type==3) { ?>
                    <div id="choose_dates">
                   <div class="box-header" style="padding-left:0; padding-top:20px;">
                   <h3 class="box-title"> Timeslot label and time slots ( to edit override the given date/time slot)</h3>
                   </div><!-- /.box-header -->
                   
                   
                       <div class="form-group">
                    <label>Input a label for the times(s) slot </label>
                      <input type="text" name="timeslotstring" class="form-control" placeholder="Eg. Every Monday And Wednesday @ 8.30-10.30 pm" id="timestring" 
                      data-msg-required="a label for the timeslot is required." data-rule-required="true" value="<? if(isset($date_string)) echo $date_string ?>" autofocus>
                    </div><!-- /.input group -->
                    <? 
                     if(count($date_array)>0)  {		
                     foreach ($date_array as $slot ) { 
					 $pieces_dte = explode(":", $slot);
					 $slot1_from=$pieces_dte[0];
                     $slot1_to=$pieces_dte[1];
					 
					 
					  ?>
                    <div class="form-group">
                    <label>Date range:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="dteTime[]" class="form-control pull-right datetimeslot" id="date"  data-msg-required="dates are required."  data-rule-required="true" 
                      value="<?=date('d/m/Y H:i', $slot1_from).' - '.date('d/m/Y H:i', $slot1_to);?>">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                      
                  <?  }  } echo ' <div class="form-group" id="parentsection">   
                  
                    <button type="button" class="btn btn-primary" onClick="addSlot();">add new time slot</button> 
                   </div> </div>   '; }  ?>
                   
                   
                    
                        <div class="form-group" style="display:none;" id="first">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_1" class="btn btn-primary" onClick="addSlot1();">add new time slot</button>
                  </div>
                 
                     
                      <div class="form-group" style="display:none;" id="second">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_2"  class="btn btn-primary" onClick="addSlot2();">add new time slot</button>
                  </div>
                 
                 
                         
                     <div class="form-group" style="display:none;" id="third">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_3"  class="btn btn-primary" onClick="addSlot3();">add new time slot</button>
                  </div>
                 
                      <div class="form-group" style="display:none;" id="fourth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_4"  class="btn btn-primary" onClick="addSlot4();">add new time slot</button>
                  </div>    
                  
                     <div class="form-group" style="display:none;" id="fifth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_5"  class="btn btn-primary" onClick="addSlot5();">add new time slot</button>
                  </div>  
                  
                   <div class="form-group" style="display:none;" id="sixth">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_6"  class="btn btn-primary" onClick="addSlot6();">add new time slot</button>
                  </div>   
                  
                    <div class="form-group" style="display:none;" id="seven">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                    <button type="button" id="button_7"  class="btn btn-primary" onClick="addSlot7();">add new time slot</button>
                  </div>     
                    
                    <div class="form-group" style="display:none;" id="eight">
                    <label>start  / end</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    <input type="text" name="dteTimeExtra[]" class="form-control pull-right datetimeslot" placeholder="please choose a start and end date for this event or special" autofocus>
                    </div><!-- /.input group -->
                    <br>
                   
                  </div>  
                  
                       
                <div class="box-header" style="margin-left:0px; padding-left:0px;">
                <h3 class="box-title" >Edit picture</h3>
                </div><!-- /.box-header -->
                
                  
                  <div class="form-group" style="margin-left:0px; padding-left:0px;">
                  <div class="col-xs-4" style="margin-left:0px; padding-left:0px;"> 
                  <? if(!empty($imageName)) echo $imageName.'.jpg'?>
                 
                  </div>
                     
                    <div class="col-xs-4" style="margin-left:0px; padding-left:0px;">
                     <a href="#" class="toggleimage" style="color:#333;"><i class="fa fa-picture-o"></i> Change Profile Image </a>
                    </div>
                    
                    </div>
                    
                   <br style="clear:both">
                   <div class="form-group" id="changepic" style="display:none;">
                  <div class="col-xs-4" style="margin-left:0px; padding-left:0px;"> 
     
                     <label for="exampleInputFile">Upload an Image / profile pic </label>
                      <input type="file" id="exampleInputFile" name="file">
                      <p class="help-block">file types accepted are (png, jpg)</p>
                      </div>
                     <br>

                    </div>               
                    <br style="clear:both;">
                    <div class="form-group">
                     
                      <textarea class="form-control" id="desc" name="discoverable_desc" rows="5" placeholder="Enter a description here  (No more than 100 words)..." data-msg-required="A description of the discoverable is required." data-rule-required="true"><? if(isset($desc)) echo $desc ?></textarea>
                      <p class="help-block" id="chars">500</p>
                    </div>    
                  <div class="box-header" style="padding-left:0;">
                  <h3 class="box-title">More details</h3>
                </div><!-- /.box-header -->
                   
                    <div class="form-group">
                      <label>Address </label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="begin typing to auto complete the location" 
                      data-msg-required="An address is required." data-rule-required="true" value="<? if(isset($address)) echo $address?>">
                    </div>
                    <div class="form-group">
                      <label>Location (lattitide and Longitude)</label>
                    </div>
                    
                    <div class="col-xs-3" style="padding-left:0;">
                      <input type="text" class="form-control" name="lat" id="lat" placeholder="latitude.." value="<? if(isset($lat)) echo $lat ?>">
                    </div>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" name="lng" id="lng"  placeholder="longitude..." value="<? if(isset($lng)) echo $lng ?>">
                    </div>
                           
                   <br style="clear:both"><br>
                    
                 
                  
                  
                   
                  
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Price</h3>
                   </div><!-- /.box-header -->
                    <div class="form-group">
                      <input type="text" class="form-control" name="cost" id="cost" placeholder="how much" value="<? if(isset($cost)) echo $cost?>"  data-msg-required="a cost is required."  data-rule-required="true">
                      
                        <input type="hidden" class="form-control" name="ID" id="ID" placeholder="how much" value="<? if(isset($object_ref)) echo $object_ref?>">
                      
                      
                    </div>
                    
                       
                      <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Contact Number</h3>
                   </div><!-- /.box-header -->
                    
                     <div class="form-group">
                      <input type="text" class="form-control" name="contact" id="contact" placeholder="contact number" value="<? if(isset($contact)) echo $contact?>"  data-msg-required="a contact number  is required." data-rule-required="true">
                    </div>
                    
                    
                       
                     <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Discoverable Tips</h3>
                   </div><!-- /.box-header -->
                   
                    <div class="form-group">   
                     <?  if(count($tips_array)>0)  {		
                      foreach ($tips_array as $item1 ) { ?>	 
                       <li class="default" style="margin-bottom:10px; list-style-type:none;"> 
                      <input placeholder="" type="text" name="tips[]" value="<?=$item1?>" width="160"/><span style="cursor: pointer;" onClick="closeMe(this);"> <i class="fa fa-close"></i></span>	
                      </li>  
					 <? } } ?>        
                    </div> 
                   
                     <ul id="list2"  style="padding-left:0;">
                    <li class="default" style="display: none; padding-left:0 padding-bottom:10px; list-style-type:none;">
                        <input placeholder="new tip" type="text" name="tipsnew[]"/><span style="cursor: pointer;" onClick="closeMe2(this);"> <i class="fa fa-close"></i></span>
                    </li>
                    </ul>
                    
                                          
                    <div class="form-group">   
                    <button type="button" class="btn btn-primary" onClick="addTip();">add a new tip</button>
                    </div> 
                    
                    

                  <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Tags / interests</h3>
                   </div><!-- /.box-header -->
                   <? 
                     if(sizeof($query_list_tags1)>0)  {		
                      foreach ($query_list_tags1 as $item1 ) { 
					  $objectRef= $item1->getObjectId(); 	
				   ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="tags[]" value="<?=$objectRef?>" <?
						   if (in_array($objectRef, $object_ref_tags)) echo "checked=checked"; ?>>
                        </span>
                        <input type="text" class="form-control" placeholder="<?=$item1->get('title')?>">
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
        
                   
                    <?  } }  ?>
                     <br style="clear:both"><br>
                    <div class="box-header" style="padding-left:0; padding-top:20px;">
                     <h3 class="box-title">Browsing Tags</h3>
                   </div><!-- /.box-header -->
                   
                    <? 
                     if(sizeof($query_list_tags2)>0)  {		
                      foreach ($query_list_tags2 as $item2 ) { 
					  $objectRef= $item2->getObjectId();  
					  
					  
					   ?>
                     <div class="col-lg-2">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input type="checkbox" name="tags2[]" value="<?=$objectRef?>"  <? if (in_array($objectRef, $btag_object)) echo "checked=checked"; ?>>
                        </span>
                        <input type="text" class="form-control" placeholder="<?=$item2->get('title')?>">
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                 
                   
                    <? } }  ?>
                   
                       
                     <br style="clear:both;">
                      <div class="form-group">
                        <div class="col-sm-1" style="float:right; margin-right:75px;">
                          <button type="submit" name="edit" class="btn btn-success btn-lrg">Edit discoverable</button>
                        </div>
                        </div>
                    
                  </form>
                  
            </div>
            </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      
        
       <? if(isset($success) && !empty($success)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Success</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$success?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
         <? }  
		 
	 if(isset($error) && !empty($error)) {  ?>
  <div class="example-modal" style="display:none;">
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Errors</h4>
                  </div>
                  <div class="modal-body">
                    <p><?=$error?></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->
          
        <? }  
      
	   include ('includes/footer.php'); 
       include ('includes/config_panel.php');
       ?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
   <?  include ('includes/js_base.php');?>
   
   
    <script>
	$(".toggleimage").click(function () {
    $("#changepic").toggle();
     });
    </script>
    
    <script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>

  <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>

 <script type="text/javascript">
    $("#editdiscoverable").validate({         
      showErrors: function(errorMap, errorList) {
          // Clean up any tooltips for valid elements
          $.each(this.validElements(), function (index, element) {
              var $element = $(element);
              $element.data("title", "") // Clear the title - there is no error associated anymore
                  .removeClass("error")
                  .tooltip("destroy");
          });
          // Create new tooltips for invalid elements
          $.each(errorList, function (index, error) {
              var $element = $(error.element);
              $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                  .data("title", error.message)
                  .addClass("error")
                  .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
          });
      }
  });
  </script>
<script>
(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

</script>  
<script>
var elem = $("#chars");
$("#crm_bio").limiter(500, elem);
</script>

<script>

function closeMe2(element) {
  $(element).parent().remove();
}

function closeMe(element) {
  $(element).parent().remove();
}

function addTip() {
  var container = $('#list2');
  var item = container.find('.default').clone();
  item.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item.appendTo(container).show();
}

	
</script>  

  <?  if(isset($success) && !empty($success)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show'); 
	 $("form").trigger("reset");
	 });
	 
	</script>

 <?  } ?>
 
  <?  if(isset($error) && !empty($error)) {  ?>
    <script>
	  $(function () {
     $('.example-modal').modal('show')  
	 });
	 
	</script>
 <?  } ?>
<script>
var elem = $("#chars2");
$("#title").limiter(100, elem);
</script>

<script>
var elem = $("#chars3");
$("#sub_title").limiter(100, elem);
</script>
     <script>
var elem = $("#chars");
$("#bio_desc").limiter(500, elem);
</script>
    <script>
      $(function () {		  
		  var mapoptions = {
		  details: "form",
		  types: ['geocode','establishment']
        };
        
		$('#address').geocomplete(mapoptions)		
       
});

  </script> 
  
  
   <script>

function closeMe2(element) {
  $(element).parent().remove();
}

function addTip() {
  var container = $('#list2');
  var item = container.find('.default').clone();
  item.removeClass('default');
  //add anything you like to item, ex: item.addClass('abc')....
  item.appendTo(container).show();
}


function addSlot() {
  $('#parentsection').hide();
  $('#first').show();
  
}

function addSlot1() {

 $('#button_1').hide();
  $('#second').show();
  
}


function addSlot2() {
 $('#button_2').hide();
  $('#third').show();
  
}

function addSlot3() {
 $('#button_3').hide();
  $('#fourth').show();
  
}

function addSlot4() {
 $('#button_4').hide();
  $('#fifth').show();
  
}

function addSlot5() {
 $('#button_5').hide();
  $('#sixth').show();
  
}

function addSlot6() {
 $('#button_6').hide();
  $('#seven').show();
  
}

function addSlot7() {
 $('#button_7').hide();
  $('#eight').show();
  
}


</script>  


  

<script>
  $(function () {
    $('.datetimeslot').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY H:mm'});

   })
    </script>  
    
<script type="text/javascript">
$(document).ready(function(){
    $("#type").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")==2){
                
                $("#choose_dates").show();
            }
            else if($(this).attr("value")==3){
              
                $("#choose_dates").show();
            }
           
            else{
                $("#choose_dates").hide();
            }
        });
    })
});
</script>
  
</body>
</html>
