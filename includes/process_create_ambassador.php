<? 
//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
//lets get the last contributor image string and the last jar id
$query_contributor = new ParseQuery("Contributor");
$query_contributor->descending("createdAt");
$query_contributor->limit(1);
$results_contributor = $query_contributor->find();
if(count($results_contributor)>0)  {		
foreach ($results_contributor as $contributor ) {  
     $jar_id=$contributor->get('jarId'); 
	 $imageName=$contributor->get('imageName');
 }
}
//format jar + 1
$jarID=$jar_id+1;
//extract numbers
$result=get_numerics($imageName);
$num1= $result[0]+1;
$num2= $result[1];
$insert1=false;
$insert2=false;
  //lets prepare the form inputs
  $keysToExtract = array('crm_name', 'crm_email', 'crm_location', 'crm_url' , 'crm_bio');
  //extract the post array
  extract($_POST);
  //loop through and get values 

  foreach ($keysToExtract as $key) {									  	      
		if(!empty($_POST[$key])) { 
			$$key =$_POST[$key];
			}
		}
		
if(!isset($_POST['access'])) { $access=$_POST['access']; $admin_level=false; } else {$admin_level=true; }
	
	//lets stick to naming conventions and format teh string val for imageString	
	$name_pieces = explode(" ", $crm_name);	
	$fname=$name_pieces[0];
	$lname=$name_pieces[1];
	$imagestring='contributor-'.$num1.'-'.$num2.'-'.$fname.'-'.$lname;
//error flag 
$errorflag=0; 
//lets process teh uploaded file resize if needed and store 
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
	 
	$tmpFile = $_FILES["file"]["tmp_name"];
    $fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];
	$fnamebody=pathinfo($fileName, PATHINFO_FILENAME);
	$fnameext=pathinfo($fileName,PATHINFO_EXTENSION);
		
	//various paths to store the images 
	$pathStore1='../ambassador-images/256/';
	$pathStore2='../ambassador-images/Lge/';
	if($fileSize > 5242880) { // if file size is larger than 5 Megabytes
       $error.= "<li>ERROR: Your file was larger than 5 Megabytes in size.</li>";
	   $errorflag=1;
       unlink($tmpFile); 
	}	
	if (!preg_match("/.(jpeg|jpg|png)$/i", $fileName) ) {
     $error.= "<li>ERROR: Your image was not  .jpg, or .jpeg or .png.</li>";
	 $errorflag=1;
     unlink($tmpFile);    
     }  
   if($errorflag==0) {
	
	
 if (file_exists('../ambassador-images/256/'.$imagestring.'.jpg')) { 
	   // echo 'exists 32';
	  unlink('../ambassador-images/256/'.$imagestring.'.jpg'); 
	 }  
   
   
   	  if (!file_exists('../ambassador-images/256/'.$imagestring.'.'.$fnameext)) { 
     
    	//move_uploaded_file($_FILES['file']['tmp_name'], $pathStore1.$fileName);
		
		
		$im = new ImageManipulator($_FILES['file']['tmp_name']);
		
		
			$width=round($im->getWidth());
			$height=round($im->getHeight());
			
			$mediumHeight = 0;
            $mediumWidth = 600;
			
			
		  	 
           if($width > 600) {
             // setup height to be proportional to the original so we don't get stretching.
             $mediumHeight = ($mediumWidth * $height) / $width;
 
             // resize the image in the manipulator class.
             $im->resample($mediumWidth, $mediumHeight);
		 
		     $im->save($pathStore2.$_FILES["file"]["name"]);
         
		    }

			
		    $offset = 0;
 
		// determine which is the longer side.
		if($width > $height) {
			// setup an offset for the width as it is larger
			$offset = ($width - $height) / 2;
			$newImage = $im->crop($offset, 0, $height + $offset, $height);
							 
		} else {
			// setup an offset for the height as it is larger
			$offset = ($height - $width) / 2;
			$newImage = $im->crop(0, $offset, $width, $width + $offset);
		}
 
             // resize the cropped, square image to a thumbnail size.
           $im->resample(200, 200);
 
		
	
			$im->save($pathStore1.$_FILES["file"]["name"]);

      
		
	//	$imgsource="http://www.jamjarapp.com.au/ambassador-images/256/" . $_FILES["file"]["name"];
		
	
	

			
	
	
        } 
   
    
	 

  } 
}	

if(!empty($crm_url)) {
//check url
$test_url=validateURL($crm_url);
if($test_url==true)  {
$crm_url=$crm_url;
}

else {
	 $error.= "<li>ERROR: The URL provided is not valid please try again</li>";
	 $errorflag=1;
}
}
//password process
$generate_pass=rand_passwd();

if($errorflag==0) {	
$new_contributor = new ParseObject("Contributor");
$new_contributor->set("URL", $crm_url);
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
$new_contributor->set("imageName", $fnamebody);
}
$new_contributor->set("jarId", $jarID);
$new_contributor->set("detail", $crm_bio);
$new_contributor->set("name", $crm_name);
$new_contributor->set("location", $crm_location);
$new_contributor->set("emailAddress", $crm_email);



try  {
  $new_contributor->save();
 // echo 'Object correctly saved and created - ' .$new_contributor->getObjectId();
  $insert1=true;
  $object_ref=$new_contributor->getObjectId();
 		
} catch (ParseException $ex) {
  $error.='<li>the contributor could not be created</li> ';
  echo $ex->getCode;
  echo $ex->getMessage;
}

if($insert1==true) {	
$new_contributor_crm = new ParseObject("CrmUsers");
$new_contributor_crm->setAssociativeArray("relObjectID", array('__type' => 'Pointer', 'className' => 'Contributor', 'objectId' => $object_ref));
$new_contributor_crm->set("username", $crm_email);
$new_contributor_crm->set("password", $generate_pass);
$new_contributor_crm->set("inactive", true);
$new_contributor_crm->set("adminAccess", $admin_level);

try  {
  $new_contributor_crm->save(); 
  $insert2=true;
		
} catch (ParseException $ex2) {
     $error.='The contributor access could not be created could not be created ';
     echo $ex2->getCode;
     echo $ex2->getMessage;
  }	
}

if($insert2==true)  {	
    //lets email the new ambassador 		
	$send_email=sendActivationEmail($crm_name, $generate_pass, $crm_email);
	if($send_email)  {		
	$success='The Ambassador has been created Successfully &amp; notified of their crm access details';
   	}
 
  }

}