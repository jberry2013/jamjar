<? 
//upload class
include('class.upload.php');
//include the use of teh classes in this script
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
//lets get the last contributor image string and the last jar id
$query_contributor = new ParseQuery("Contributor");
$query_contributor->descending("createdAt");
$query_contributor->limit(1);
$results_contributor = $query_contributor->find();
if(count($results_contributor)>0)  {		
foreach ($results_contributor as $contributor ) {  
     $jar_id=$contributor->get('jarId'); 
	 $imageName=$contributor->get('imageName');
 }
}
//format jar + 1
$jarID=$jar_id+1;
//extract numbers
$result=get_numerics($imageName);
$num1= $result[0]+1;
$num2= $result[1];
$insert1=false;
$insert2=false;
  //lets prepare the form inputs
  $keysToExtract = array('crm_name', 'crm_email', 'crm_location', 'crm_url' , 'crm_bio');
  //extract the post array
  extract($_POST);
  //loop through and get values 

  foreach ($keysToExtract as $key) {									  	      
		if(!empty($_POST[$key])) { 
			$$key =$_POST[$key];
			}
		}
		
if(!isset($_POST['access'])) { $access=$_POST['access']; $admin_level=false; } else {$admin_level=true; }
	
	//lets stick to naming conventions and format teh string val for imageString	
	$name_pieces = explode(" ", $crm_name);	
	$fname=$name_pieces[0];
	$lname=$name_pieces[1];
	$imagestring='contributor-'.$num1.'-'.$num2.'-'.$fname.'-'.$lname;
//error flag 
$errorflag=0; 
//lets process teh uploaded file resize if needed and store 
if (isset($_FILES["file"]) &&  $_FILES['file']['size']>0 ) {
	 
	$tmpFile = $_FILES["file"]["tmp_name"];
    $fileName = $_FILES["file"]["name"];
	$fileSize = $_FILES["file"]["size"];	
	//various paths to store the images 
	$pathStore1='../uploads/32/';
	$pathStore2='../uploads/64/';
	$pathStore3='../uploads/128/';
	
	if($fileSize > 5242880) { // if file size is larger than 5 Megabytes
       $error.= "<li>ERROR: Your file was larger than 5 Megabytes in size.</li>";
	   $errorflag=1;
       unlink($tmpFile); 
	}	
	if (!preg_match("/.(jpeg|jpg|png)$/i", $fileName) ) {
     $error.= "<li>ERROR: Your image was not  .jpg, or .jpeg or .png.</li>";
	 $errorflag=1;
     unlink($tmpFile);    
     }  
   if($errorflag==0) {
	
	
	  if (file_exists('../uploads/32/'.$imagestring.'.jpg')) { 
	   // echo 'exists 32';
	  unlink('../uploads/32/'.$imagestring.'.jpg'); 
	 }  
     $handle1 = new Upload($_FILES['file']);     
     if ($handle1->uploaded) {
        $handle1->image_resize          = true;
		$handle1->image_ratio           = true;
		$handle1->image_y               = 32;		
		$handle1->image_x               = 32;
		$handle1->file_new_name_body = $imagestring;
        $handle1->Process($pathStore1);
      }
      if (file_exists('../uploads/64/'.$imagestring.'.jpg')) { 
	  //  echo 'exists 64';
	  unlink('../uploads/64/'.$imagestring.'.jpg'); 
	 }  
	 
	 $handle2 = new Upload($_FILES['file']);     
     if ($handle2->uploaded) {
        $handle2->image_resize          = true;
		$handle2->image_ratio           = true;
		$handle2->file_new_name_body = $imagestring;
        $handle2->Process($pathStore2);
			
	 }
	  if (file_exists('../uploads/128/'.$imagestring.'.jpg')) { 
	  //echo 'exists 128';
	  unlink('../uploads/128/'.$imagestring.'.jpg'); 
	 }  
	 $handle3 = new Upload($_FILES['file']);     
     if ($handle3->uploaded) {
        $handle3->image_resize          = true;
		$handle3->image_ratio           = true;
	
		$handle3->file_new_name_body = $imagestring;
        $handle3->Process($pathStore3);
     }	 
	 
	    if ($handle3->uploaded) {
        $handle3->image_resize          = true;
		$handle3->image_ratio           = true;
	
		$handle3->file_new_name_body = $imagestring;
        $handle3->Process($pathStore3);
     }	 
  } 
}	

if(!empty($crm_url)) {
//check url
$test_url=validateURL($crm_url);
if($test_url==true)  {
$crm_url=$crm_url;
}

else {
	 $error.= "<li>ERROR: The URL provided is not valid please try again</li>";
	 $errorflag=1;
}
}
//password process
$generate_pass=rand_passwd();

if($errorflag==0) {	
$new_contributor = new ParseObject("Contributor");
$new_contributor->set("URL", $crm_url);
$new_contributor->set("imageName", $imagestring);
$new_contributor->set("jarId", $jarID);
$new_contributor->set("detail", $crm_bio);
$new_contributor->set("name", $crm_name);
$new_contributor->set("location", $crm_location);
$new_contributor->set("emailAddress", $crm_email);



try  {
  $new_contributor->save();
 // echo 'Object correctly saved and created - ' .$new_contributor->getObjectId();
  $insert1=true;
  $object_ref=$new_contributor->getObjectId();
 		
} catch (ParseException $ex) {
  $error.='<li>the contributor could not be created</li> ';
  echo $ex->getCode;
  echo $ex->getMessage;
}

if($insert1==true) {	
$new_contributor_crm = new ParseObject("CrmUsers");
$new_contributor_crm->setAssociativeArray("relObjectID", array('__type' => 'Pointer', 'className' => 'Contributor', 'objectId' => $object_ref));
$new_contributor_crm->set("username", $crm_email);
$new_contributor_crm->set("password", $generate_pass);
$new_contributor_crm->set("inactive", true);
$new_contributor_crm->set("adminAccess", $admin_level);

try  {
  $new_contributor_crm->save(); 
  $insert2=true;
		
} catch (ParseException $ex2) {
     $error.='The contributor access could not be created could not be created ';
     echo $ex2->getCode;
     echo $ex2->getMessage;
  }	
}

if($insert2==true)  {	
    //lets email the new ambassador 		
	$send_email=sendActivationEmail($crm_name, $generate_pass, $crm_email);
	if($send_email)  {		
	$success='The Ambassador has been created Successfully &amp; notified of their crm access details';
   	}
 
  }

}