 <? if (file_exists('../ambassador-images/256/'.$_SESSION['userImage'].'.jpg')) { $imageuser='../ambassador-images/256/'.$_SESSION['userImage'].'.jpg';	    }  
else  { $imageuser='dist/img/no_pic2.png'; }	?>
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=$imageuser?>" class="img-circle" alt="User Image"  style="width:48px; height:48px;" />
            </div>
            <div class="pull-left info">
              <p><?=$_SESSION['name'];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>           
              <li <? if($pageid=='dashboard') { 
			  echo 'class="active treeview"'; } 
			  else { echo 'class="treeview"'; }
			  ?>>
              <a href="dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>            
            </li>
            
             <li  <? if($pageid=='manage-profile') { 
			  echo 'class="active treeview"'; } 
			  else { echo 'class="treeview"'; }
			  ?>>
              <a href="manage-profile">
                <i class="fa fa-user"></i> <span>Your Profile</span></i>
              </a>          
            </li>
             <? if($_SESSION['admin_level']==true) { ?>
            <li  <? if($pageid=='manage-ambassadors') { 
			  echo 'class="active treeview"'; } 
			  else { echo 'class="treeview"'; }
			  ?>>
              <a href="manage-ambassadors">
                <i class="fa fa-users"></i> <span>Manage Ambassadors</span></i>
              </a>     
            </li>
            <?  }  ?>
            
            
            <li  <? if($pageid=='manage-discoverables') { 
			  echo 'class="active treeview"'; } 
			  else { echo 'class="treeview"'; }
			  ?>>
              <? if($_SESSION['admin_level']==true) { ?>
              <a href="manage-discoverables">
           
                <i class="fa fa-building"></i> <span>Manage Discoverables</span></i> 
                </a>
               <?  } else { ?>
                <a href="manage-discoverables?admin=n">  <i class="fa fa-building"></i> <span>Manage Your Discoverables</span></i> </a>   
              <?  } ?>
              
            </li>  
            
              <? if($_SESSION['admin_level']==true) { ?>      
            <li  <? if($pageid=='manage-tags') { 
			  echo 'class="active treeview"'; } 
			  else { echo 'class="treeview"'; }
			  ?>>
              <a href="manage-tags">
                <i class="fa fa-tags"></i> <span>Manage Tags</span></i>
              </a>    
            </li>
            
            <?  }  ?>                  
            
          <?php /*?>   <? if($_SESSION['admin_level']==true) { ?>      
            <li  <? if($pageid=='manage-beacons') { 
			  echo 'class="active treeview"'; } 
			  else { echo 'class="treeview"'; }
			  ?>>
              <a href="manage-beacons">
                <i class="fa fa fa-cogs"></i> <span>Manage Beacons</span></i>
              </a>    
            </li>
            
            <?  }  ?>                  <?php */?>
          </ul>
        </section>